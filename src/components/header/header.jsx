import React, { useEffect } from 'react';
import { Avatar, Dropdown, PageHeader, Typography, Menu } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useLocation } from 'react-router';
import { useDispatch } from 'react-redux';

import avatar from '../../assets/images/avatar.png';

import './header.scss';
import { routesWithTitles } from '../sidebar/menuLinks';
import { useSelector } from 'react-redux';
import { setAccount } from '../../store/reducers/account';
import accountService from '../../services/http/accountService';
import authService from '../../services/http/authService';

function HeaderComponent() {
  const location = useLocation();
  const dispatch = useDispatch();

  const account = useSelector((state) => state.account);

  useEffect(() => {
    accountService
      .getAccount()
      .then((data) => dispatch(setAccount(data.data)))
      .catch((e) => console.log(e));
  }, []);

  const menu = (
    <Menu>
      <Menu.Item
        key="1"
        icon={<UserOutlined />}
        onClick={() => {
          authService.logout();
        }}
      >
        Выйти
      </Menu.Item>
    </Menu>
  );

  return (
    <PageHeader className="header">
      <Typography.Title level={3}>
        {routesWithTitles.find((item) => item.link === location.pathname)
          ?.label || 'ANOR PAY'}
      </Typography.Title>
      <div className="avatar-wrapper">
        <Typography.Title
          level={5}
        >{`${account?.firstName} ${account?.lastName}`}</Typography.Title>
        <Dropdown overlay={menu}>
          <Avatar size={44} src={<img src={avatar} alt="avatar" />} />
        </Dropdown>
      </div>
    </PageHeader>
  );
}

export default HeaderComponent;
