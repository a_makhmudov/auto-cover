import React from 'react';

export const Icon = ({ content }) => <img src={content} alt="" />;
