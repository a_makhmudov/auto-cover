import {
  AppstoreOutlined,
  FileDoneOutlined,
  HistoryOutlined,
  IdcardOutlined,
  PicCenterOutlined,
  SettingOutlined,
  UserOutlined,
} from '@ant-design/icons';
import React from 'react';
import {
  ROLE_AUTOCOVER_ADMIN,
  ROLE_AUTOCOVER_USER,
} from '../../helpers/roles/roles';

export const menuLinks = [
  {
    label: 'Панель управления',
    link: '/',
    icon: <AppstoreOutlined />,
    permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
  },
  {
    label: 'Клиенты',
    link: '/clients',
    icon: <PicCenterOutlined />,
    permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
  },
  {
    label: 'Долги',
    link: '/debts',
    icon: <UserOutlined />,
    permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
  },
  {
    label: 'Платежи',
    link: '/payments',
    icon: <FileDoneOutlined />,
    permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
  },
  {
    label: 'Карты',
    link: null,
    icon: <IdcardOutlined />,
    items: [
      {
        label: 'Uzcard',
        link: '/card/uzcard',
        icon: '',
        permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
      },
      {
        label: 'HUMO',
        link: '/card/humo',
        icon: '',
        permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
      },
    ],
    permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
  },
  {
    label: 'Настройки',
    link: null,
    // icon: <EllipsisOutlined />,
    icon: <SettingOutlined />,
    items: [
      {
        label: 'Горячие кнопки',
        link: '/settings/action-buttons',
        icon: (
          <div
            className="submenu-icon"
            style={{ border: '2px solid #885AF8' }}
          ></div>
        ),
        permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
      },
      {
        label: 'Лимит',
        link: '/limit',
        icon: (
          <div
            className="submenu-icon"
            style={{ border: '2px solid #2ED47A' }}
          ></div>
        ),
        permissions: [ROLE_AUTOCOVER_ADMIN],
      },
      {
        label: 'Расписание методов',
        link: '/taskScheduling',
        icon: (
          <div
            className="submenu-icon"
            style={{ border: '2px solid #5D5FEF' }}
          ></div>
        ),
        permissions: [ROLE_AUTOCOVER_ADMIN],
      },
    ],
    permissions: [ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER],
  },
  {
    label: 'История данных',
    link: null,
    icon: <HistoryOutlined />,
    items: [
      {
        label: 'Изменение данных',
        link: '/historyChange',
        icon: (
          <div
            className="submenu-icon"
            style={{ border: '2px solid #FFB946' }}
          ></div>
        ),
        permissions: [ROLE_AUTOCOVER_ADMIN],
      },
      {
        label: 'Middleware',
        link: '/historyMiddleware',
        icon: (
          <div
            className="submenu-icon"
            style={{ border: '2px solid #885AF8' }}
          ></div>
        ),
        permissions: [ROLE_AUTOCOVER_ADMIN],
      },
    ],
    permissions: [ROLE_AUTOCOVER_ADMIN],
  },
];

export const routesWithTitles = [
  {
    label: 'Панель управления',
    link: '/',
  },
  {
    label: 'Клиенты',
    link: '/clients',
  },
  {
    label: 'Долги',
    link: '/debts',
  },
  {
    label: 'Платежи',
    link: '/payments',
  },
  {
    label: 'Uzcard',
    link: '/card/uzcard',
  },
  {
    label: 'HUMO',
    link: '/card/humo',
  },
  {
    label: 'Терминал',
    link: '/terminals',
  },
  {
    label: 'Роли',
    link: '/roles',
  },
  {
    label: 'Лимит',
    link: '/limit',
  },
  {
    label: 'Расписание методов',
    link: '/taskScheduling',
  },
  {
    label: 'Изменение данных',
    link: '/historyChange',
  },
  {
    label: 'Middleware',
    link: '/historyMiddleware',
  },
];
