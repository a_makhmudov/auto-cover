import React, { useState } from 'react';
import { Divider, Layout, Menu, Typography } from 'antd';
import './sidebar.scss';
import { menuLinks } from './menuLinks';
import { Link } from 'react-router-dom';
import local from '../../helpers/local';
import { checkPermission } from '../role-based-ui/roleBasedUi';

const { Sider } = Layout;
const { Title } = Typography;
const { SubMenu } = Menu;

function Sidebar() {
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Sider
      // collapsible
      className="sidebar fixed-sidebar"
      breakpoint="lg"
      collapsed={collapsed}
      onCollapse={() => setCollapsed(!collapsed)}
      width="350"
    >
      <Title level={1} className="page-title">
        {local.translate('anor/pay')}
      </Title>
      <Divider orientation="center" className="divider" />

      <Menu className="menu" mode="inline">
        {menuLinks.map((item) => {
          if (item.link !== null) {
            return (
              checkPermission(item.permissions) && (
                <Menu.Item
                  className="menu-item"
                  icon={item.icon}
                  key={item.link}
                >
                  <Link to={item.link}>{item.label}</Link>
                </Menu.Item>
              )
            );
          } else {
            return (
              checkPermission(item.permissions) && (
                <SubMenu key={item.label} icon={item.icon} title={item.label}>
                  {item.items.map((i) => {
                    return (
                      checkPermission(i.permissions) && (
                        <Menu.Item className="menu-item" key={i.label}>
                          <Link to={i.link}>
                            {i.icon}
                            {i.label}
                          </Link>
                        </Menu.Item>
                      )
                    );
                  })}
                </SubMenu>
              )
            );
          }
        })}
      </Menu>
    </Sider>
  );
}

export default Sidebar;
