import React from 'react';
import { DatePicker, Input, Select } from 'antd';
import './filterFields.scss';
import { FILTER_FIELD_TYPES } from '../../helpers/appConstants';
import { DateMask } from '../../pages/history/dateMask';

const { Option } = Select;

export const FilterFieldItem = ({ field, value, setValue }) => {
  function onChange(value) {
    const dateValue = DateMask(value?._d);
    setValue(dateValue);
  }

  switch (field.type) {
    case FILTER_FIELD_TYPES.TEXT:
      return (
        <div>
          <div className="filter-field">
            <label htmlFor="" className="filter-field__label">
              {field.label}
            </label>
            <Input
              className="filter-field__input"
              type="text"
              placeholder={field.placeholder}
              value={value}
              onChange={(e) => setValue(e.target.value)}
            />
          </div>
        </div>
      );
    case FILTER_FIELD_TYPES.SELECT:
      return (
        <div className="filter-field">
          <label htmlFor="" className="filter-field__label">
            {field.label}
          </label>
          <Select
            className="filter-field__select"
            value={value}
            allowClear
            onSelect={(v) => setValue(v)}
          >
            {field?.options?.items.map((item) => (
              <Option key={`${item.value}`} value={`${item.value}`}>
                {item.label}
              </Option>
            ))}
          </Select>
        </div>
      );
    case FILTER_FIELD_TYPES.DATE:
      return (
        <div className="filter-field">
          <label htmlFor="" className="filter-field__label">
            {field.label}
          </label>
          <DatePicker
            className="filter-field__date-picker"
            format={'YYYY-MM-DD'}
            onChange={onChange}
            // onOk={onOk}
            // value={moment(value, 'YYYY-MM-DD')}
            placement={'topRight'}
            placeholder={value ? value : field.placeholder}
          />
        </div>
      );
    default:
      return (
        <div className="filter-field">
          <label htmlFor="" className="filter-field__label">
            {field.label}
          </label>
          <Input
            className="filter-field__input"
            type="text"
            placeholder={field.label}
            value={value}
            onChange={(e) => setValue(e.target.value)}
          />
        </div>
      );
  }
};
