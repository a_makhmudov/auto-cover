import React, { useEffect, useState } from 'react';
import { FilterFieldItem } from './filterFieldItem';
import { Button } from 'antd';
import './filterFields.scss';
import local from '../../helpers/local';

export const FilterFields = ({ fields, filter, defaultParams }) => {
  const [dataState, setDataState] = useState({});

  useEffect(() => {
    const initialState = {};
    fields.forEach((item) => {
      initialState[item.dataIndex] = defaultParams[item.reqKey] || '';
    });
    setDataState(initialState);
  }, []);

  const clearFilterFields = () => {
    const initialState = {};
    fields.forEach((item) => {
      initialState[item.dataIndex] = '';
    });
    setDataState(initialState);
  };

  const createAndSendFilterParam = () => {
    const reqObj = {};
    fields.forEach((field) => {
      if (dataState[field.dataIndex]) {
        reqObj[field.reqKey] = dataState[field.dataIndex];
      }
    });
    filter(reqObj);
  };

  return (
    <div>
      <div>
        {fields.map((fieldItem) => {
          return (
            <FilterFieldItem
              key={fieldItem.label}
              field={fieldItem}
              value={dataState[fieldItem.dataIndex]}
              setValue={(value) =>
                setDataState({
                  ...dataState,
                  [fieldItem.dataIndex]: value,
                })
              }
            />
          );
        })}
      </div>
      <div className="filter-field__btn">
        <Button type="danger" onClick={clearFilterFields}>
          {local.translate('client/filter/clear')}
        </Button>
        &nbsp;
        <Button type="primary" onClick={createAndSendFilterParam}>
          {local.translate('client/filter')}
        </Button>
      </div>
    </div>
  );
};
