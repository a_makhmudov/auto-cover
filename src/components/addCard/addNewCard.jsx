import React, { useEffect, useRef, useState } from 'react';

import { message } from 'antd';
import './addNewCard.scss';
import Button from 'antd/es/button';
import clientService from '../../services/http/clientService';
import { useParams } from 'react-router-dom';
import local from '../../helpers/local/index';
import CurrencyFormat from 'react-currency-format';

export const AddNewCard = ({ handeOk, handeCancel }) => {
  const [data, setData] = useState({});
  const { clientId } = useParams(); // clientId - for get current client data
  const formRef = useRef();

  const [cardData, setCardData] = useState({
    pan: '',
    expDate: '',
    expDateValid: false,
    panValid: false,
  });

  //-------------------- Regex for validate Inputs value ---------------------//
  const regExp = {
    cardPan: new RegExp('^[1-9][0-9]{15}$'),
    cardExp: new RegExp('^(0[1-9]|1[0-2])\\/?([0-9]{4}|[0-9]{2})$'),
  };
  //-------------------- Regex for validate Inputs value ---------------------//

  //-------------------- Get current client data ---------------------------//
  useEffect(() => {
    clientService.getClientDetailsByClientId(clientId).then((res) => {
      res = data;
      setData(res?.data);
    });
  }, []);
  //-------------------- Get current client data ---------------------------//

  //-------------- Call service and send request to create(update) new client card -----------------------//
  const addNewClientCard = async () => {
    try {
      const cardDataWithoutMask = {
        pan: cardData.pan.replace(/\s/g, ''),
        expDate: cardData.expDate.replace('/', ''),
      };
      await clientService.addNewClientCard(clientId, cardDataWithoutMask);
      handeOk(true);
      setCardData({
        pan: '',
        expDate: '',
        expDateValid: null,
        panValid: null,
      });
      message.success('Card added successfully', 6);
      handeCancel(false);
    } catch (e) {
      message.error(e.message);
      handeCancel(false);
    }
  };

  //-------------- Call service and send request to create(update) new client card -----------------------//

  //----------------------  onChange Inputs settings---------------------//

  const setCardPan = (event) => {
    let cardPan = event.target.value.replace(/\s/g, '');
    if (regExp.cardPan.test(cardPan)) {
      setCardData({ ...cardData, pan: event.target.value, panValid: true });
    } else {
      setCardData({ ...cardData, pan: event.target.value, panValid: false });
    }
  };
  const setCardExp = (event) => {
    let cardExp = event.target.value.replace('/', '');
    if (regExp.cardExp.test(cardExp)) {
      setCardData({
        ...cardData,
        expDate: event.target.value,
        expDateValid: true,
      });
    } else {
      setCardData({
        ...cardData,
        expDate: event.target.value,
        expDateValid: false,
      });
    }
  };
  //----------------------  onChange Inputs settings---------------------//

  //----------------------  Card expiry date setting---------------------//
  function limit(val, max) {
    if (val.length === 1 && val[0] > max[0]) {
      val = '0' + val;
    }

    if (val.length === 2) {
      if (Number(val) === 0) {
        val = '01';

        //this can happen when user paste number
      } else if (val > max) {
        val = max;
      }
    }

    return val;
  }

  function cardExpiry(val) {
    let month = limit(val.substring(0, 2), '12');
    let year = val.substring(2, 4);

    return month + (year.length ? '/' + year : '');
  }

  //----------------------  Card expiry date setting---------------------//

  return (
    <div className="add-new-card">
      <form ref={formRef}>
        <div className="add-new-card__input-element card-num">
          <label htmlFor="">
            {local.translate('client/card/number')} &nbsp;
            <sup className="required-star">*</sup>
          </label>
          <div>
            <CurrencyFormat
              format="#### #### #### ####"
              onChange={setCardPan}
              placeholder="0000 0000 0000 0000"
              style={{ borderColor: cardData.panValid ? '' : '#FF5254' }}
            />
            {cardData.panValid ? (
              ''
            ) : (
              <span className="add-new-card__invalid">
                {local.translate('client/card/add/invalid/pan')}
              </span>
            )}
          </div>
        </div>

        <div className="add-new-card__input-element card-exp">
          <label htmlFor="">
            {local.translate('client/card/term')} &nbsp;
            <sup className="required-star">*</sup>
          </label>
          <div>
            <CurrencyFormat
              format={cardExpiry}
              onChange={setCardExp}
              placeholder={local.translate('client/card/expDate')}
              style={{ borderColor: cardData.expDateValid ? '' : '#FF5254' }}
            />
            {cardData.expDateValid ? (
              ''
            ) : (
              <span className="add-new-card__invalid">
                {local.translate('client/card/add/invalid/expDate')}
              </span>
            )}
          </div>
        </div>

        <div className="add-new-card__btns">
          <Button type="danger" onClick={() => handeCancel(false)}>
            {local.translate('common/actions/cancel')}
          </Button>
          &nbsp;
          <Button
            disabled={!cardData.panValid || !cardData.expDateValid}
            type="primary"
            onClick={() => addNewClientCard()}
          >
            {local.translate('common/actions/save')}
          </Button>
        </div>
      </form>
    </div>
  );
};
