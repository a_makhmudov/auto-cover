import React from 'react';
import { Button, Col, Row, Table, Typography } from 'antd';
import { ArrowLeftOutlined, ArrowRightOutlined } from '@ant-design/icons';
import './dataTable.scss';
import { ClearBtn } from './actionButtons/clearBtn';
import { FilterBtn } from './actionButtons/filterBtn';
import { CustomButton } from './actionButtons/importBtn';
import local from '../../helpers/local';

function DataTable(props) {
  const {
    rowKey,
    columns,
    data,
    title,
    filterBtnClickHandler,
    deleteBtnClickHandler,
    importBtnClickHandler,
    clearBtnClickHandler,
    rowSelection,
    hasSelected,
    pagination,
    buttonLabel,
  } = props;
  return (
    <Table
      rowSelection={rowSelection}
      rowKey={(r) => r[rowKey]}
      className="data-table"
      columns={columns}
      dataSource={data}
      scroll={{ x: 1400 }}
      title={() => (
        <Row justify="space-between" align="middle">
          <Col>
            <Typography.Title level={5}>{title}</Typography.Title>
          </Col>
          <Col style={{ display: 'flex' }}>
            {clearBtnClickHandler && (
              <ClearBtn clearBtnClickHandler={clearBtnClickHandler} />
            )}

            {filterBtnClickHandler && (
              <FilterBtn filterBtnClickHandler={filterBtnClickHandler} />
            )}
            {deleteBtnClickHandler && (
              <Button
                type="danger"
                className={`action-button button `}
                onClick={deleteBtnClickHandler}
                disabled={hasSelected}
              >
                {local.translate('client/delete')}
              </Button>
            )}
            {importBtnClickHandler && (
              <CustomButton
                buttonLabel={buttonLabel}
                btnClickHandler={importBtnClickHandler}
              />
            )}
          </Col>
        </Row>
      )}
      pagination={
        pagination && {
          ...pagination,
          itemRender: (page, type) => {
            if (type === 'prev') {
              return (
                <Button shape="circle">
                  <ArrowLeftOutlined />
                </Button>
              );
            }
            if (type === 'next') {
              return (
                <Button shape="circle">
                  <ArrowRightOutlined />
                </Button>
              );
            }
            if (type === 'page') {
              return <Button shape="circle">{page}</Button>;
            }
            return <Button shape="circle">...</Button>;
          },
        }
      }
    />
  );
}

export default DataTable;
