import local from '../../../helpers/local';
import { Button } from 'antd';
import React from 'react';

export const FilterBtn = ({ filterBtnClickHandler }) => {
  return (
    <Button
      type="primary"
      className="action-button"
      onClick={filterBtnClickHandler}
    >
      {local.translate('client/filter')}
    </Button>
  );
};
