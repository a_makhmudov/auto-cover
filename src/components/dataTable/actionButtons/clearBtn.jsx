import local from '../../../helpers/local';
import { Button } from 'antd';
import React from 'react';

export const ClearBtn = ({ clearBtnClickHandler }) => {
  return (
    <div>
      <Button
        type={'primary'}
        className="action-button"
        onClick={clearBtnClickHandler}
        style={{ marginRight: 10 }}
      >
        {local.translate('client/filter/updateClean')}
      </Button>
    </div>
  );
};
