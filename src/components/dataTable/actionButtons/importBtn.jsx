import React from 'react';
import { Button } from 'antd';

export const CustomButton = ({ btnClickHandler, buttonLabel }) => {
  return (
    <Button className="action-button button--import" onClick={btnClickHandler}>
      {buttonLabel}
    </Button>
  );
};
