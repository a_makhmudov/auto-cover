import local from '../../../helpers/local';
import { Button, Modal } from 'antd';
import React, { useState } from 'react';
import { WarningTwoTone } from '@ant-design/icons';

export const DeleteBtn = ({ disableBtn, deleteBtnClickHandler }) => {
  //---------------------- Modal settings --------------------------//
  const [isModalVisible, setIsModalVisible] = useState(false);
  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleOk = async () => {
    await deleteBtnClickHandler();
    setIsModalVisible(false);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  //---------------------- Modal settings --------------------------//
  return (
    <div>
      <Modal
        style={{ textAlign: 'center' }}
        title={local.translate('debit/confirmation/delete')}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <WarningTwoTone style={{ fontSize: '30px' }} twoToneColor={'#FF5254'} />
        &nbsp;
        <span>{local.translate('debit/confirm/delete')}</span>
      </Modal>
      <Button
        type="danger"
        className={`action-button button--${
          disableBtn ? 'delete' : 'disabled'
        }`}
        style={{ marginRight: 10 }}
        onClick={showModal}
        disabled={disableBtn}
      >
        {local.translate('client/delete')}
      </Button>
    </div>
  );
};
