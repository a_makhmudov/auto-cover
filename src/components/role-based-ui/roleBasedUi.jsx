import jwtDecode from 'jwt-decode';

export const checkPermission = (roles) => {
  let jwt = localStorage.getItem('access_token');

  const decode_jwt = jwtDecode(jwt);
  let hasPermission = false;

  roles.forEach((role) => {
    if (decode_jwt?.authorities?.includes(role)) {
      hasPermission = true;
    }
  });

  return hasPermission;
};

export const RoleBasedUi = ({ roles, children }) => {
  let jwt = localStorage.getItem('access_token');
  const decode_jwt = jwtDecode(jwt);
  let hasPermission = false;

  roles.forEach((role) => {
    if (decode_jwt?.authorities?.includes(role)) {
      hasPermission = true;
    }
  });

  return hasPermission ? children : null;
};
