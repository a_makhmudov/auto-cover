import CurrencyFormat from 'react-currency-format';

export const Currency = ({ number }) => {
  const value = number / 100;
  return (
    <CurrencyFormat
      displayType={'text'}
      value={value}
      thousandSeparator={' '}
    />
  );
};
