import $dataApi from './dataApi';

class LimitService {
  createOrUpdateLimit(clientCountLimit, limitSum) {
    return $dataApi.post('/services/autocover/api/limit', {
      clientCountLimit: clientCountLimit,
      limitSum: limitSum,
    });
  }

  getLimits() {
    return $dataApi.get('/services/autocover/api/limit');
  }
}

export default new LimitService();
