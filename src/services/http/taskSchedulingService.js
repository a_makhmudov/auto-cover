import $dataApi from './dataApi';

class TaskSchedulingService {
  getTasksList() {
    return $dataApi.get('/services/autocover/api/task-schedulings/all');
  }

  changeStatusTask(taskId) {
    return $dataApi.get(
      `/services/autocover/api/task-schedulings/status/${taskId}`
    );
  }
  resetTasksList() {
    return $dataApi.get('/services/autocover/api/task-schedulings/reset');
  }

  stopRunningSchedule() {
    return $dataApi.get(
      '/services/autocover/api/task-schedulings/change-all-status',
      {
        params: {
          status: false,
        },
      }
    );
  }

  continueRunningSchedule() {
    return $dataApi.get(
      '/services/autocover/api/task-schedulings/change-all-status',
      {
        params: {
          status: true,
        },
      }
    );
  }

  getAllScheduleStatus() {
    return $dataApi.get('/services/autocover/api/task-schedulings/all-status');
  }
}

export default new TaskSchedulingService();
