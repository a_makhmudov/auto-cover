import $dataApi from './dataApi';

class PaymentService {
  getPaymentsList(filterData) {
    return $dataApi.get('/services/autocover/api/history', {
      params: {
        ...filterData,
        sort: 'date,desc',
      },
    });
  }
  getAllPaymentsSum() {
    return $dataApi.get('/services/autocover/api/history/payment');
  }

  getAllResidueSum() {
    return $dataApi.get('/services/autocover/api/history/residue');
  }
  getAllHandleSum() {
    return $dataApi.get('/services/autocover/api/history/handle');
  }

  downloadPaymentList(beginDate, endDate) {
    return $dataApi.post(
      '/services/autocover/api/client/export',
      {
        beginDate: beginDate,
        endDate: endDate,
      },
      { responseType: 'blob' }
    );
  }
}

export default new PaymentService();
