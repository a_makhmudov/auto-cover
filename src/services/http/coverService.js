import $dataApi from './dataApi';

class CoverService {
  coversRun() {
    return $dataApi.get('/services/autocover/api/cover/run');
  }

  checkCoverStatus() {
    return $dataApi.get('services/autocover/api/cover/check-status');
  }

  coverStartStop() {
    return $dataApi.get('services/autocover/api/cover/start/stop');
  }

  getCoverListByMonth(date) {
    return $dataApi.post('services/autocover/api/history/one-month', {
      date: date,
    });
  }

  getCoverListByDay(date) {
    return $dataApi.post('services/autocover/api/history/one-day', {
      date: date,
    });
  }

  getCoverListByHalfYear() {
    return $dataApi.get('services/autocover/api/history/half-year');
  }
}

export default new CoverService();
