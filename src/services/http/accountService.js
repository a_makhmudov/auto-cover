import $dataApi from './dataApi';

class AccountService {
  getAccount() {
    return $dataApi.get('/services/pkuaa/api/account');
  }
}

export default new AccountService();
