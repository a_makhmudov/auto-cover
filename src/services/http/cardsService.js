import $dataApi from './dataApi';

const BASE_URL = `/services/autocover/api`;

class CardsService {
  getAdditionalCardsList(filterParams) {
    return $dataApi.get(`/services/autocover/api/additional-card/all`, {
      params: filterParams,
    });
  }

  importAdditionCardsList(file) {
    const formData = new FormData();
    formData.append('file', file);
    return $dataApi.post(`${BASE_URL}/additional-card/import`, formData);
  }

  deleteUzcards(cardsList) {
    return $dataApi.delete(`${BASE_URL}/additional-card/delete`, {
      data: cardsList,
    });
  }
}

export default new CardsService();
