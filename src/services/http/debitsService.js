import $dataApi from './dataApi';

class DebitsService {
  getDebits(filterParams) {
    return $dataApi.get('/services/autocover/api/debit', {
      params: filterParams,
    });
  }
  deleteDebit(debitId) {
    return $dataApi.delete(`/services/autocover/api/debit/${debitId}`);
  }

  getTotalDebitSum() {
    return $dataApi.get('/services/autocover/api/debit/total-sum');
  }
}

export default new DebitsService();
