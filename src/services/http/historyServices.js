import $dataApi from './dataApi';

class HistoryServices {
  getHistoryMiddleware() {
    return $dataApi.get('services/autocover/api/request-histories');
  }

  getAllChangesHistory(page, pageSize) {
    return $dataApi.get('services/autocover/api/action-histories', {
      params: {
        sort: `id,desc`,
        page,
        size: pageSize,
      },
    });
  }
  getChangesHistoryById(historyId) {
    return $dataApi.get(`services/autocover/api/action-histories/${historyId}`);
  }
  getChangeHistoryCount() {
    return $dataApi.get('services/autocover/api/action-histories/count');
  }
}

export default new HistoryServices();
