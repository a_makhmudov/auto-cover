import eventBus from '../../helpers/eventBus';
import $dataApi from './dataApi';

class AuthService {
  login(body) {
    return $dataApi.post('/auth/login', body);
  }

  logout() {
    eventBus.dispatch('logout', {});
  }
}

export default new AuthService();
