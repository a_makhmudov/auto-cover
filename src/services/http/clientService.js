import $dataApi from './dataApi';

class ClientService {
  getClients(filterParams) {
    return $dataApi.get('/services/autocover/api/client', {
      params: filterParams,
    });
  }

  getClientDetailsByClientId(clientId) {
    return $dataApi.get(`/services/autocover/api/client/${clientId}`);
  }

  deleteClients(clientsId) {
    return $dataApi.delete('/services/autocover/api/client', {
      data: clientsId,
    });
  }

  importClientList(file) {
    const formData = new FormData();
    formData.append('file', file);
    return $dataApi.post(`/services/autocover/api/client/import`, formData);
  }

  changeAutoCoverStatus(clientId) {
    return $dataApi.post(
      `/services/autocover/api/client/is-auto-cover/${clientId}`
    );
  }

  getClientDebitByClientId(clientId) {
    return $dataApi.get(`/services/autocover/api/client/debit/${clientId}`);
  }

  getClientsByCriteria(filterParams) {
    return $dataApi.get('/services/autocover/api/clients/criteria', {
      params: filterParams,
    });
  }

  getClientPaymentDetails(clientId) {
    return $dataApi.get(
      `/services/autocover/api/request-histories/${clientId}`
    );
  }

  coverClientAllDebits(clientId) {
    return $dataApi.get(`/services/autocover/api/cover/${clientId}`);
  }

  coverClientCard(clientId, cardDetails) {
    return $dataApi.post(`/services/autocover/api/cover/${clientId}`, {
      balance: cardDetails.balance,
      pan: cardDetails.pan,
    });
  }

  getClientCoverHistory(clientId) {
    return $dataApi.get(`/services/autocover/api/history/${clientId}`);
  }

  getClientsByPage(pageNum) {
    return $dataApi.get('/services/autocover/api/clients', {
      params: {
        page: pageNum,
        size: 20,
      },
    });
  }

  // *****************************  Card API ******************************

  getClientCardsByClientId(clientId) {
    return $dataApi.get(`/services/autocover/api/card/${clientId}`);
  }

  addNewClientCard(clientId, body) {
    return $dataApi.post(`/services/autocover/api/card/${clientId}`, {
      expDate: body.expDate,
      pan: body.pan,
    });
  }

  deleteClientCard(cardPan) {
    return $dataApi.delete(`/services/autocover/api/card/${cardPan}`);
  }

  updateCardBalance(cardPan) {
    return $dataApi.put(
      `/services/autocover/api/card/update-balance/${cardPan}`
    );
  }

  getByPinfl(clientId, pinfl) {
    return $dataApi.post('/services/autocover/api/card/pinFl', {
      clientId,
      pinfl,
    });
  }

  getCardByPassport(body) {
    return $dataApi.post('/services/autocover/api/card/passport', { body });
  }

  getCoverHistoryByMonth() {
    return $dataApi.get('/services/autocover/api/history/cover/history');
  }
}

export default new ClientService();
