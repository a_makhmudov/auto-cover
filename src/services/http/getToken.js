import jwtDecode from 'jwt-decode';

export function getToken() {
  const token = localStorage.getItem('access_token');
  const tokenBody = jwtDecode(token);
  return tokenBody;
}
