import axios from 'axios';
import authService from './authService';
import { BASE_URL } from '../../helpers/appConstants';

const $dataApi = axios.create({
  baseURL:
    // eslint-disable-next-line no-undef
    process.env.NODE_ENV === 'development' ? '' : BASE_URL,
});

$dataApi.interceptors.request.use((config) => {
  const login_data = JSON.parse(localStorage.getItem('login_data'));
  if (login_data?.access_token) {
    config.headers.Authorization = `Bearer ${login_data?.access_token}`;
  }
  return config;
});

$dataApi.interceptors.response.use(
  (res) => {
    return res;
  },
  async (err) => {
    const originalConfig = err.config;

    if (originalConfig.url !== '/auth/login' && err.response) {
      if (err.response.status === 401) {
        authService.logout();
      }
    }

    return Promise.reject(err);
  }
);

export default $dataApi;
