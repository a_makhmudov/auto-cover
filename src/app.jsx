import React, { useEffect } from 'react';
import { Layout } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import './app.scss';
import AppRouter from './routing/appRouter';
import HeaderComponent from './components/header/header';
import Sidebar from './components/sidebar/sidebar';
import { resetUserData, setUser } from './store/reducers/user';
import eventBus from './helpers/eventBus';
import { resetAccountData } from './store/reducers/account';

function App() {
  const user = useSelector((state) => state.user);
  const login_data = JSON.parse(localStorage.getItem('login_data'));
  const dispatch = useDispatch();

  useEffect(() => {
    eventBus.on('logout', () => {
      localStorage.clear();
      dispatch(resetAccountData());
      dispatch(resetUserData());
    });
    return () => {
      eventBus.remove('logout', () => {});
    };
  }, []);

  if (login_data?.access_token && !user.isAuth) {
    dispatch(
      setUser({
        ...login_data,
        isAuth: true,
      })
    );
  }

  return (
    <Layout className="app" hasSider>
      {user.isAuth && <Sidebar />}
      <Layout>
        {user.isAuth && <HeaderComponent />}
        <Layout.Content className="content">
          <AppRouter />
        </Layout.Content>
      </Layout>
    </Layout>
  );
}

export default App;
