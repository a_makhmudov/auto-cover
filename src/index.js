import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ErrorBoundary from './components/errorBoundary/errorBoundary';
import { BrowserRouter as Router } from 'react-router-dom';
import { store } from './store';
import App from './app';
import { CookiesProvider } from 'react-cookie';

ReactDOM.render(
  <CookiesProvider>
    <Provider store={store}>
      <ErrorBoundary>
        <Router>
          <App />
        </Router>
      </ErrorBoundary>
    </Provider>
  </CookiesProvider>,
  document.getElementById('root')
);
