import { createSlice } from '@reduxjs/toolkit';

const clientDetailsSlice = createSlice({
  name: 'clientDetails',
  initialState: {
    details: {},
    debits: [],
    cards: [],
    payments: [],
  },
  reducers: {
    setClientDetails(state, action) {
      state.details = action.payload.details;
    },
    setClientDebits(state, action) {
      state.debits = action.payload.debits;
    },
    setClientPayments(state, action) {
      state.payments = action.payload.payments;
    },
    setClientCards(state, action) {
      state.cards = action.payload.cards;
    },
  },
});

export const {
  setClientDetails,
  setClientDebits,
  setClientPayments,
  setClientCards,
} = clientDetailsSlice.actions;

export default clientDetailsSlice.reducer;
