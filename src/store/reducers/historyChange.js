import { createSlice } from '@reduxjs/toolkit';

const historyChangeSlice = createSlice({
  name: 'historyChange',
  initialState: {
    content: [],
  },
  reducers: {
    setHistoryChange(state, action) {
      state.content = action.payload;
    },
  },
});

export const { setHistoryChange } = historyChangeSlice.actions;
export default historyChangeSlice.reducer;
