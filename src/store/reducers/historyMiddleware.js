import { createSlice } from '@reduxjs/toolkit';

const historyMiddlewareSlice = createSlice({
  name: 'middleware',
  initialState: {
    data: [],
  },
  reducers: {
    setHistoryMiddleWare(state, action) {
      state.data = action.payload;
    },
  },
});

export const { setHistoryMiddleWare } = historyMiddlewareSlice.actions;
export default historyMiddlewareSlice.reducer;
