import { createSlice } from '@reduxjs/toolkit';

const uzCardSlice = createSlice({
  name: 'uzCard',
  initialState: {
    content: [],
    filterData: {},
    importUzcardList: {},
    currentPage: 0,
    empty: false,
    pageable: {},
    totalPages: null,
    totalElements: null,
    last: false,
    first: true,
    size: 20,
    number: 0,
    numberOfElements: 0,
  },
  reducers: {
    setUzcard(state, action) {
      state.content = action.payload.content;
      state.pageable = action.payload.pageable;
      state.totalPages = action.payload.totalPages;
      state.totalElements = action.payload.totalElements;
      state.last = action.payload.last;
      state.first = action.payload.first;
      state.size = action.payload.size;
      state.number = action.payload.number;
      state.numberOfElements = action.payload.numberOfElement;
      state.empty = action.payload.empty;
    },
    setImportUzcardList(state, action) {
      state.importUzcardList = action.payload;
    },
    setUzcardFilterData(state, action) {
      state.filterData = action.payload;
    },
    setCurrentPage(state, action) {
      state.currentPage = action.payload;
    },
    resetUzCardData(state) {
      state.content = [];
      state.pageable = {};
      state.totalPages = null;
      state.totalElements = null;
      state.last = false;
      state.first = true;
      state.size = 20;
      state.number = 0;
      state.numberOfElements = 0;
      state.empty = false;
    },
  },
});

export const {
  setUzcard,
  resetUzCardData,
  setUzcardFilterData,
  setCurrentPage,
  setImportUzcardList,
} = uzCardSlice.actions;

export default uzCardSlice.reducer;
