import { createSlice } from '@reduxjs/toolkit';

const debitsSlice = createSlice({
  name: 'debts',
  initialState: {
    content: [],
    filterData: {},
    currentPage: 0,
    empty: true,
    pageable: {},
    totalElements: 0,
    totalPages: 0,
    last: true,
    first: true,
    size: 0,
    number: 0,
    numberOfElements: 0,
    sort: {
      empty: true,
      sorted: true,
      unsorted: true,
    },
  },
  reducers: {
    setDebits(state, action) {
      state.content = action.payload.content;
      state.empty = action.payload.empty;
      state.content = action.payload.content;
      state.pageable = action.payload.pageable;
      state.totalPages = action.payload.totalPages;
      state.totalElements = action.payload.totalElements;
      state.last = action.payload.last;
      state.first = action.payload.first;
      state.size = action.payload.size;
      state.number = action.payload.number;
      state.numberOfElements = action.payload.numberOfElement;
    },
    setDebitsCurrentPage(state, action) {
      state.currentPage = action.payload;
    },
    setDebtFilterData(state, action) {
      state.filterData = action.payload;
    },
    resetDebitsData(state) {
      state.content = [];
      state.pageable = {};
      state.totalPages = null;
      state.totalElements = null;
      state.last = false;
      state.first = true;
      state.size = 20;
      state.number = 0;
      state.numberOfElements = 0;
      state.empty = false;
    },
  },
});

export const {
  setDebits,
  resetDebitsData,
  setDebitsCurrentPage,
  setDebtFilterData,
} = debitsSlice.actions;

export default debitsSlice.reducer;
