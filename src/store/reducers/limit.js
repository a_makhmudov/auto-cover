import { createSlice } from '@reduxjs/toolkit';

const limitSlice = createSlice({
  name: 'limits',
  initialState: {
    clientCountLimit: 0,
    limitSum: 0,
    id: 0,
  },
  reducers: {
    setLimits(state, action) {
      state.clientCountLimit = action.payload.clientCountLimit;
      state.limitSum = action.payload.limitSum;
      state.id = action.payload.id;
    },
  },
});

export const { setLimits } = limitSlice.actions;

export default limitSlice.reducer;
