import { createSlice } from '@reduxjs/toolkit';

const accountSlice = createSlice({
  name: 'account',
  initialState: {
    activated: false,
    authorities: [],
    detail: null,
    email: '',
    firstName: '',
    groups: [],
    id: 0,
    imageUrl: '',
    lastName: '',
    login: '',
    phoneNumber: '',
  },
  reducers: {
    setAccount(state, action) {
      state.activated = action.payload.activated;
      state.authorities = action.payload.authorities;
      state.detail = action.payload.detail;
      state.email = action.payload.email;
      state.firstName = action.payload.firstName;
      state.groups = action.payload.groups;
      state.id = action.payload.id;
      state.imageUrl = action.payload.imageUrl;
      state.lastName = action.payload.lastName;
      state.login = action.payload.login;
      state.phoneNumber = action.payload.phoneNumber;
    },
    resetAccountData(state) {
      state.activated = false;
      state.authorities = [];
      state.detail = null;
      state.email = '';
      state.firstName = '';
      state.groups = [];
      state.id = 0;
      state.imageUrl = '';
      state.lastName = '';
      state.login = '';
      state.phoneNumber = '';
    },
  },
});

export const { setAccount, resetAccountData } = accountSlice.actions;

export default accountSlice.reducer;
