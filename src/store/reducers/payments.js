import { createSlice } from '@reduxjs/toolkit';

const paymentsSlice = createSlice({
  name: 'payments',
  initialState: {
    content: [],
    filterData: {},
    currentPage: 0,
    pageable: {},
    totalElements: null,
    totalPages: null,
    last: true,
    first: true,
    number: 0,
    numberOfElements: 0,
    size: 20,
    empty: false,
  },
  reducers: {
    setPayments(state, action) {
      state.content = action.payload.content;
      state.pageable = action.payload.pageable;
      state.totalPages = action.payload.totalPages;
      state.totalElements = action.payload.totalElements;
      state.sort = action.payload.sort;
      state.last = action.payload.last;
      state.first = action.payload.first;
      state.size = action.payload.size;
      state.number = action.payload.number;
      state.numberOfElements = action.payload.numberOfElement;
      state.empty = action.payload.empty;
    },

    setPaymentsCurrentPage(state, action) {
      state.currentPage = action.payload;
    },

    setPaymentsFilterData(state, action) {
      state.filterData = action.payload;
    },
  },
});

export const { setPayments, setPaymentsCurrentPage, setPaymentsFilterData } =
  paymentsSlice.actions;

export default paymentsSlice.reducer;
