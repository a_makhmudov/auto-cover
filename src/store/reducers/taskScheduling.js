import { createSlice } from '@reduxjs/toolkit';

const taskSchedulingSlice = createSlice({
  name: 'taskScheduler',
  initialState: {
    data: [],
  },
  reducers: {
    setScheduling(state, action) {
      state.data = action.payload;
    },
  },
});

export const { setScheduling } = taskSchedulingSlice.actions;

export default taskSchedulingSlice.reducer;
