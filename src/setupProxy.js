const { createProxyMiddleware } = require('http-proxy-middleware');

// eslint-disable-next-line no-undef
module.exports = function (app) {
  app.use(
    ['/auth', '/api', '/services'],
    createProxyMiddleware({
      target: 'http://000.000.000.0',
      changeOrigin: true,
    })
  );
};
