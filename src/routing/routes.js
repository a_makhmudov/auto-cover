import ClientDetailsPage from '../pages/clientDetails/clientDetails';
import ClientsPage from '../pages/clients/clients';
import DebtsPage from '../pages/debts/debits';
import HomePage from '../pages/home/home';
import LimitPage from '../pages/limit/limit';
import LoginPage from '../pages/login/login';
import NotFoundPage from '../pages/notFound/notFound';
import PaymentsPage from '../pages/payments/payments';
import TerminalsPage from '../pages/terminal/terminal';
import {
  CARDS_HUMO_PAGE,
  CARDS_UZCARD_PAGE,
  CHANGE_HISTORY_ROUTE,
  CLIENTS_ROUTE,
  DEBTS_ROUTE,
  HOME_ROUTE,
  LIMIT_ROUTE,
  LOGIN_ROUTE,
  MIDDLEWARE_ROUTE,
  PAYMENTS_ROUTE,
  SETTINGS_ACTION_BUTTONS,
  TASK_SCHEDULING_ROUTE,
  TERMINALS_ROUTE,
} from './appRouteConstants';
import TaskSchedulingPage from '../pages/taskScheduling/taskScheduling';
import HistoryMiddleWare from '../pages/history/historyMiddleWare';
import HistoryChange from '../pages/history/changeHistory';
import CardUzcardPage from '../pages/cards/uzcard/uzcard';
import CardHumoPage from '../pages/cards/humo/humo';
import { SettingsActionButtons } from '../pages/settingsActionButtons/settingsActionButtons';

export const PERMISSIONS = {
  READ: 'READ',
  WRITE: 'WRITE',
};

export const publicRoutes = [
  {
    path: LOGIN_ROUTE,
    component: <LoginPage />,
    permission: [],
  },
];

export const authRoutes = [
  {
    path: HOME_ROUTE,
    component: <HomePage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: CLIENTS_ROUTE,
    component: <ClientsPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: `${CLIENTS_ROUTE}/:clientId`,
    component: <ClientDetailsPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: DEBTS_ROUTE,
    component: <DebtsPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: PAYMENTS_ROUTE,
    component: <PaymentsPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: TERMINALS_ROUTE,
    component: <TerminalsPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: SETTINGS_ACTION_BUTTONS,
    component: <SettingsActionButtons />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: LIMIT_ROUTE,
    component: <LimitPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: TASK_SCHEDULING_ROUTE,
    component: <TaskSchedulingPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: CHANGE_HISTORY_ROUTE,
    component: <HistoryChange />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: MIDDLEWARE_ROUTE,
    component: <HistoryMiddleWare />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: CARDS_UZCARD_PAGE,
    component: <CardUzcardPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: CARDS_HUMO_PAGE,
    component: <CardHumoPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
  {
    path: '*',
    component: <NotFoundPage />,
    permission: [PERMISSIONS.READ, PERMISSIONS.WRITE],
  },
];
