import ru from './ru.json';
import uz from './uz.json';

export const LANGUAGES = {
  RU: 'RU',
  UZ: 'UZ',
};

const language = LANGUAGES.RU;

const translate = (key) => {
  switch (language) {
    case LANGUAGES.RU: {
      return ru[key];
    }
    case LANGUAGES.UZ: {
      return uz[key];
    }
    default:
      return ru[key];
  }
};
export default {
  translate,
};
