import checkIcon from '../assets/icons/icon-check.svg';
import unCheckIcon from '../assets/icons/icon-redRound.svg';

const APP_ICONS = {
  checkIcon,
  unCheckIcon,
};

export default APP_ICONS;
