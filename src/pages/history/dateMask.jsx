export const DateMask = (newDate) => {
  let newData = new Date(newDate) || '';
  let year;
  let month;
  let day;
  if (newData) {
    year = newData.getFullYear().toString();
    month = newData.getMonth() + 1;
    day = newData.getDate();
  }

  if (month < 10) {
    const fullMonth = `0${month}`;
    month = fullMonth;
  }
  if (day < 10) {
    let fullDay = `0${day}`;
    day = fullDay;
  }

  return `${year}-${month}-${day}`;
};
