import React, { useEffect } from 'react';
import DataTable from '../../components/dataTable/dataTable';
import { historyMiddleware } from '../../helpers/tableColumns';
import { useDispatch, useSelector } from 'react-redux';
import { setHistoryMiddleWare } from '../../store/reducers/historyMiddleware';
import historyServices from '../../services/http/historyServices';

const HistoryMiddleWare = () => {
  const dispatch = useDispatch();
  useEffect(async () => {
    const middlewareList = await historyServices.getHistoryMiddleware();
    dispatch(setHistoryMiddleWare(middlewareList.data));
  }, []);
  const historyMiddlewareList = useSelector((state) => state.historyMiddleware);
  return (
    <DataTable
      rowKey={'startDate'}
      columns={historyMiddleware}
      data={historyMiddlewareList?.data || null}
      pagination={false}
    />
  );
};

export default HistoryMiddleWare;
