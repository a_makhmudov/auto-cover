import React, { useEffect, useState } from 'react';
import { historyChangeColumns } from '../../helpers/tableColumns';
import historyServices from '../../services/http/historyServices';
import { useDispatch, useSelector } from 'react-redux';
import { setHistoryChange } from '../../store/reducers/historyChange';
import DataTable from '../../components/dataTable/dataTable';
import { Button } from 'antd';
import local from '../../helpers/local';

const HistoryChange = () => {
  const [currentPage, setCurrentPage] = useState(0);
  const [total, setTotal] = useState(0);
  const dispatch = useDispatch();
  const historyChangeList = useSelector((state) => state.historyChange);
  useEffect(async () => {
    const response = await historyServices.getAllChangesHistory(
      currentPage,
      20
    );

    setTotal(response?.headers['x-total-count']);
    dispatch(setHistoryChange(response.data));
  }, [currentPage]);

  return (
    <div>
      <DataTable
        className="data-table"
        rowKey={'created_at'}
        columns={historyChangeColumns}
        data={historyChangeList.content || null}
        pagination={{
          onChange: (page) => {
            setCurrentPage(page - 1);
          },
          showSizeChanger: false,
          pageSize: 20,
          total: Number(total) || null,
          current: currentPage + 1,
          showQuickJumper: {
            goButton: (
              <Button>
                {local.translate('anor/pay/table/pagination/jumper')}
              </Button>
            ),
          },
        }}
      />
    </div>
  );
};

export default HistoryChange;
