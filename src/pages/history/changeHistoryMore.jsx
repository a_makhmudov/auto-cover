import React, { useState } from 'react';
import { Col, List, Modal } from 'antd';
import Button from 'antd/es/button';
import historyServices from '../../services/http/historyServices';
import { historyChangeColumnsMore } from '../../helpers/tableColumns';
import { DateMaskHistory } from './dateHistoryMask';

export const ChangeHistoryMore = ({ text }) => {
  const [historyChangeMore, setHistoryChangeMore] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const getChangeHistoryByHistoryId = async () => {
    showModal();
    const response = await historyServices.getChangesHistoryById(text.id);
    setHistoryChangeMore(response.data);
  };

  return (
    <div>
      <Modal
        width={1000}
        title="Подробно"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <List
          itemLayout="vertical"
          size="large"
          dataSource={historyChangeColumnsMore}
          renderItem={(item) => {
            if (item.dataIndex === 'created_at') {
              return (
                <List.Item style={{ display: 'flex' }}>
                  <Col span={12}>
                    <b>{item.title}:</b>
                  </Col>
                  <div>
                    <DateMaskHistory
                      record={historyChangeMore[item.dataIndex]?.toString()}
                    />
                  </div>
                </List.Item>
              );
            }
            return (
              <List.Item style={{ display: 'flex' }}>
                <Col span={12}>
                  <b>{item.title}:</b>
                </Col>
                <div>{historyChangeMore[item.dataIndex]?.toString()}</div>
              </List.Item>
            );
          }}
        />
      </Modal>
      <Button onClick={() => getChangeHistoryByHistoryId()}>
        Подробне ...
      </Button>
    </div>
  );
};
