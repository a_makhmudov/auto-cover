import paymentsService from '../../services/http/paymentService';

export async function getPaymentList(filterParams) {
  const paymentList = await paymentsService.getPaymentsList(filterParams);
  return paymentList.data;
}
