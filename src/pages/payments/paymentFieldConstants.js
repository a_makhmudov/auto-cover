import { FILTER_FIELD_TYPES } from '../../helpers/appConstants';

export const paymentFields = [
  {
    label: 'Паспорт серия  :',
    placeholder: 'Введите паспорт серия',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'passport',
    reqKey: 'passport',
  },
  // {
  //   label: 'Паспорт ID',
  //   placeholder: 'Введите Пасспорт ID',
  //   type: FILTER_FIELD_TYPES.TEXT,
  //   textDataField: '',
  //   dataIndex: 'clientId',
  //   reqKey: 'clientId.contains',
  // },
  {
    label: 'ФИО :',
    placeholder: 'Введите ФИО',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'fio',
    reqKey: 'fio',
  },
  {
    label: 'Номер карты :',
    placeholder: 'Введите номер карты',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'pan',
    reqKey: 'pan',
  },
  {
    label: 'Сумма :',
    placeholder: 'Введите сумму',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: null,
    dataIndex: 'amount',
    reqKey: 'amount',
  },
  {
    label: 'Дата :',
    placeholder: 'Введите дату',
    type: FILTER_FIELD_TYPES.DATE,
    defaultValue: '',
    dataIndex: 'totalDebit',
    reqKey: 'date',
  },
];
