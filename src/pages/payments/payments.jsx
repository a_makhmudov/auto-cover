import React, { useEffect, useState } from 'react';
import { Alert, Button, Card, DatePicker, message, Modal, Space } from 'antd';

import './payments.scss';
import DataTable from '../../components/dataTable/dataTable';
import { useDispatch, useSelector } from 'react-redux';
import {
  setPayments,
  setPaymentsCurrentPage,
  setPaymentsFilterData,
} from '../../store/reducers/payments';
import { paymentColumns } from '../../helpers/tableColumns';
import local from '../../helpers/local';
import { FilterFields } from '../../components/filterFields/filterFields';
import Spinner from '../../components/spinner/spinner';
import { ReloadOutlined } from '@ant-design/icons';
import { paymentFields } from './paymentFieldConstants';
import paymentService from '../../services/http/paymentService';

function PaymentsPage() {
  const dispatch = useDispatch();
  const paymentsList = useSelector((state) => state.payments);
  const [isFilterFieldModal, setFilterFieldModal] = useState(false); // state for filter modal window
  const [exportModalVisible, setExportModalVisible] = useState(false); // state for filter modal window
  const currentPage = useSelector((state) => state.payments.currentPage);
  const paymentsFilterParams = useSelector(
    (state) => state.payments.filterData
  );
  const [loading, setLoading] = useState(false);
  const [hasFilterData, setHasFiltersData] = useState(false);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  useEffect(async () => {
    await loadPaymentsListPerPage(currentPage);
  }, [currentPage, paymentsFilterParams]);

  const getPaymentList = async () => {
    dispatch(setPaymentsFilterData({}));
    dispatch(setPaymentsCurrentPage(0));
  };

  const filterData = async (filterData) => {
    const modifiedFilterData = {
      ...filterData,
      fio: filterData?.fio?.toUpperCase(),
    };
    dispatch(setPaymentsFilterData(modifiedFilterData));
    dispatch(setPaymentsCurrentPage(0));
  };

  const loadPaymentsListPerPage = async (page) => {
    try {
      setLoading(true);
      const response = await paymentService.getPaymentsList({
        ...paymentsFilterParams,
        page: page,
      });
      if (response.data.content.length <= 0) {
        message.error(local.translate('client/filter/noData'));
        dispatch(setPayments([]));
        setHasFiltersData(true);
        setLoading(false);
        setFilterFieldModal(false);
      } else {
        setFilterFieldModal(false);
        setLoading(false);
        dispatch(setPayments(response.data));
        message.success(local.translate('payments/get/paymentsList/success'));
        setHasFiltersData(false);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const exportPaymentList = async () => {
    try {
      const response = await paymentService.downloadPaymentList(
        startDate,
        endDate
      );
      const a = document.createElement('a');
      a.href = URL.createObjectURL(response.data);
      a.download = 'payment-list.xlsx';
      a.click();
    } catch (e) {
      console.log(e);
    }
  };

  function onChangeStartDatePicker(date, dateString) {
    setStartDate(dateString);
  }

  function onChangeEndDatePicker(date, dateString) {
    setEndDate(dateString);
  }

  if (loading) {
    return <Spinner />;
  }
  return paymentsList?.content?.length > 0 ? (
    <Card className="payments">
      <Modal
        title={local.translate('client/filter')}
        visible={isFilterFieldModal}
        onOk={() => getPaymentList()}
        onCancel={() => setFilterFieldModal(false)}
        footer={null}
      >
        <FilterFields
          defaultParams={paymentsFilterParams}
          fields={paymentFields}
          filter={filterData}
          closeFilterModal={() => setFilterFieldModal(false)}
        />
      </Modal>
      <Modal
        title={local.translate('payments/export')}
        visible={exportModalVisible}
        onOk={() => exportPaymentList()}
        onCancel={() => setExportModalVisible(false)}
        footer={null}
        className="export-modal-buttons"
      >
        <h4>Дата начало</h4>
        <DatePicker
          placeholder={'Введите дату начало'}
          style={{ width: '100%', height: '30px' }}
          onChange={onChangeStartDatePicker}
        />
        <h4>Дата окончание</h4>
        <DatePicker
          placeholder={'Введите дату окончания'}
          style={{ width: '100%', height: '30px' }}
          onChange={onChangeEndDatePicker}
        />
        <Space
          style={{ margin: '10px 0', width: '100%' }}
          direction={'vertical'}
        >
          <Button
            className="action-button button--import"
            style={{ width: '100%' }}
            disabled={!startDate || !endDate}
            onClick={() => exportPaymentList()}
          >
            {local.translate('anor/pay/download')}
          </Button>
          <Button
            className="action-button button--delete"
            style={{ width: '100%' }}
            onClick={() => setExportModalVisible(false)}
          >
            {local.translate('common/actions/cancel')}
          </Button>
        </Space>
      </Modal>

      <DataTable
        buttonLabel={local.translate('anor/pay/download')}
        rowKey="amount"
        columns={paymentColumns}
        data={paymentsList.content || null}
        title={
          <>
            <strong>{local.translate('anor/pay/count')}:</strong>
            &nbsp;
            {paymentsList.totalElements}
          </>
        }
        pagination={{
          current: currentPage + 1,
          pageSize: 20,
          total: paymentsList?.totalElements || null,
          showSizeChanger: false,
          showLessItems: true,
          showQuickJumper: {
            goButton: (
              <Button>
                {local.translate('anor/pay/table/pagination/jumper')}
              </Button>
            ),
          },
          onChange: (page) => dispatch(setPaymentsCurrentPage(page - 1)),
        }}
        filterBtnClickHandler={() => setFilterFieldModal(true)}
        clearBtnClickHandler={() => getPaymentList()}
        importBtnClickHandler={() => setExportModalVisible(true)}
      />
    </Card>
  ) : (
    <div className="no-data">
      <Alert
        message={local.translate('client/alert/warning')}
        type="warning"
        showIcon
        closable
        description={
          hasFilterData
            ? local.translate('client/filter/serverError')
            : local.translate('client/filter/noData')
        }
      />
      <br />
      <Button
        type={'primary'}
        onClick={() => getPaymentList()}
        icon={<ReloadOutlined />}
      >
        {local.translate('payments/get/paymentsList')}
      </Button>
    </div>
  );
}

export default PaymentsPage;
