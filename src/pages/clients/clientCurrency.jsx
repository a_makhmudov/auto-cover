import CurrencyFormat from 'react-currency-format';

export const ClientCurrency = ({ record }) => {
  return (
    <CurrencyFormat
      displayType={'text'}
      value={record?.totalDebit / 100 || 0}
      thousandSeparator={' '}
    />
  );
};
