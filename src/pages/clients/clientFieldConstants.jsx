import { FILTER_FIELD_TYPES } from '../../helpers/appConstants';
import checkIcon from '../../assets/icons/icon-check.svg';
import unCheckIcon from '../../assets/icons/icon-redRound.svg';
import { Icon } from '../../components/Icon/icon';
export const clientFields = [
  {
    label: 'ПИНФЛ :',
    placeholder: 'Введите ПИНФЛ',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'pinfl',
    reqKey: 'pinfl',
  },
  // {
  //   label: 'Паспорт ID',
  //   placeholder: 'Введите Пасспорт ID',
  //   type: FILTER_FIELD_TYPES.TEXT,
  //   textDataField: '',
  //   dataIndex: 'clientId',
  //   reqKey: 'clientId.contains',
  // },
  {
    label: 'ФИО :',
    placeholder: 'Введите ФИО',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'fio',
    reqKey: 'fio',
  },
  {
    label: 'Телефон :',
    placeholder: 'Введите телефон номер',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'phone',
    reqKey: 'phone',
  },
  {
    label: 'Автосписание :',
    placeholder: 'Введите Автосписание',
    type: FILTER_FIELD_TYPES.SELECT,
    options: {
      items: [
        {
          label: <Icon content={unCheckIcon} />,
          value: false,
        },
        {
          label: <Icon content={checkIcon} />,
          value: true,
        },
      ],
    },
    defaultValue: null,
    dataIndex: 'isAutoCover',
    reqKey: 'isAutoCover',
  },
  {
    label: 'Текущий долг :',
    placeholder: 'Введите Текущий долг',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'totalDebit',
    reqKey: 'totalDebit',
  },
];
