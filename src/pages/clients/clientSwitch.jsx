import React from 'react';
import { Switch } from 'antd';
import clientService from '../../services/http/clientService';

export const ClientSwitch = ({ record }) => {
  const changeStatus = async () => {
    await clientService.changeAutoCoverStatus(record.clientId);
  };

  return (
    <Switch
      defaultChecked={record?.isAutoCover}
      onChange={() => changeStatus()}
    />
  );
};
