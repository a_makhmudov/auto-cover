import {
  Alert,
  Button,
  Card,
  Col,
  Form,
  Input,
  message,
  Modal,
  Row,
  Table,
} from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DataTable from '../../components/dataTable/dataTable';
import Spinner from '../../components/spinner/spinner';
import {
  setClientFilterData,
  setClients,
  setCurrentPage,
} from '../../store/reducers/client';

import './clients.scss';
import { clientFields } from './clientFieldConstants';
import { FilterFields } from '../../components/filterFields/filterFields';
import local from '../../helpers/local/index';
import { deleteClients } from './clientRequests';
import { clientsColumns } from '../../helpers/tableColumns';
import clientService from '../../services/http/clientService';
import jwtDecode from 'jwt-decode';
import { ROLE_AUTOCOVER_ADMIN } from '../../helpers/roles/roles';

function ClientsPage() {
  const [isModalVisible, setIsModalVisible] = useState(false); // state for close or open modal window
  const [isFilterFieldModal, setFilterFieldModal] = useState(false); // state for filter modal window
  const [loading, setLoading] = useState(false);
  const [hasFilterData, setHasFiltersData] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); // state for count of selected rows
  const [hasSelectedRow, setHasSelectedRow] = useState(false);
  const hasSelected = selectedRowKeys.length > 0; // this variable for disable  delete client button
  const clientsFilterParams = useSelector((state) => state.clients.filterData);
  const currentPage = useSelector((state) => state.clients.currentPage);
  const [importFile, setImportFile] = useState(null);
  const dispatch = useDispatch();
  const clients = useSelector((state) => state.clients);
  const [isAdminRole, setIsAdminRole] = useState(false);

  useEffect(async () => {
    await loadClientsListPerPage(currentPage);
    /* This functional for hide row selection */
    let jwt = localStorage.getItem('access_token');
    const decode_jwt = jwtDecode(jwt);
    if (decode_jwt.authorities.includes(ROLE_AUTOCOVER_ADMIN)) {
      setIsAdminRole(true);
    } else setIsAdminRole(false);
    /* This functional for hide row selection */
  }, [currentPage, clientsFilterParams]);

  const getClientList = async () => {
    dispatch(setClientFilterData({}));
    dispatch(setCurrentPage(0));
  };

  const deleteBtnClicksHandler = async (clientsId) => {
    const ids = clientsId.map((item) => {
      return { clientId: item };
    });
    const deleteResponse = await deleteClients(ids);
    if (deleteResponse.success) {
      setSelectedRowKeys([]);
      await loadClientsListPerPage(currentPage);
      message.success(local.translate('client/delete-success'));
      setHasSelectedRow(true);
    } else {
      message.error(local.translate('client/delete-error'));
      setHasSelectedRow(false);
    }
  };

  const filterData = async (filterData) => {
    const modifiedFilterData = {
      ...filterData,
      fio: filterData?.fio?.toUpperCase(),
    };
    dispatch(setClientFilterData(modifiedFilterData));
    dispatch(setCurrentPage(0));
  };

  const loadClientsListPerPage = async (page) => {
    setHasSelectedRow(false);
    try {
      setLoading(true);
      const response = await clientService.getClients({
        ...clientsFilterParams,
        page: page,
      });
      if (response.data.content.length <= 0) {
        message.error(local.translate('client/filter/noData'));
        dispatch(setClients([]));
        setHasFiltersData(true);
        setLoading(false);
        setFilterFieldModal(false);
      } else {
        setFilterFieldModal(false);
        setLoading(false);
        dispatch(setClients(response.data));
        message.success(local.translate('client/clientList/success'));
        setHasFiltersData(false);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const importClientList = async () => {
    await clientService.importClientList(importFile);
    setIsModalVisible(false);
  };

  //------------------- This function will call when select one or more rows   -----------------//
  const onSelectChange = (selectedRowKeys) => {
    setSelectedRowKeys(selectedRowKeys);
  };
  //------------------- This function will call when select one or more rows -----------------//

  //---------------------- select all or invert all data setting -----------------//
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [Table.SELECTION_ALL, Table.SELECTION_NONE],
  };
  //---------------------- select all or invert all data setting -----------------//

  if (loading) {
    return <Spinner />;
  }

  return clients?.content?.length > 0 ? (
    <Card className="clients">
      <Modal
        title={local.translate('client/filter')}
        visible={isFilterFieldModal}
        onOk={() => filterData(clientsFilterParams)}
        onCancel={() => setFilterFieldModal(false)}
        footer={null}
      >
        <FilterFields
          defaultParams={clientsFilterParams}
          fields={clientFields}
          filter={filterData}
          closeFilterModal={() => setFilterFieldModal(false)}
        />
      </Modal>

      <Modal
        title={local.translate('client/import/data')}
        visible={isModalVisible}
        onOk={() => {}}
        onCancel={() => setIsModalVisible(false)}
        footer={null}
      >
        <Form>
          <Form.Item name="file">
            <Input
              type="file"
              name="file"
              onChange={(e) => {
                setImportFile(e.target.files[0]);
              }}
            />
          </Form.Item>
          <Row justify="end">
            <Col span={5} offset={13}>
              <Button
                type="ghost"
                size="large"
                onClick={() => setIsModalVisible(false)}
              >
                {local.translate('client/import/cancel')}
              </Button>
            </Col>
            <Col span={5} offset={1}>
              <Button
                htmlType="submit"
                type="primary"
                size="large"
                onClick={() => importClientList()}
              >
                {local.translate('client/import/ok')}
              </Button>
            </Col>
          </Row>
        </Form>
      </Modal>

      <DataTable
        buttonLabel={local.translate('client/import')}
        hasSelected={!hasSelected}
        hasSelectedRow={hasSelectedRow}
        rowKey={'clientId'}
        columns={clientsColumns}
        data={clients?.content || null}
        title={`${local.translate('client/clientsCount')} ${
          clients.totalElements
        }`}
        filterBtnClickHandler={() => setFilterFieldModal(true)}
        clearBtnClickHandler={() => getClientList()}
        deleteBtnClickHandler={
          isAdminRole ? () => deleteBtnClicksHandler(selectedRowKeys) : null
        }
        importBtnClickHandler={() => setIsModalVisible(true)}
        rowSelection={isAdminRole ? rowSelection : null}
        pagination={{
          current: currentPage + 1,
          pageSize: 20,
          total: clients.totalElements || null,
          showSizeChanger: false,
          showLessItems: true,
          showQuickJumper: {
            goButton: (
              <Button>
                {local.translate('anor/pay/table/pagination/jumper')}
              </Button>
            ),
          },
          onChange: (page) => {
            dispatch(setCurrentPage(page - 1));
          },
        }}
      />
    </Card>
  ) : (
    <div className="no-data">
      <Alert
        message={local.translate('client/alert/warning')}
        type="warning"
        showIcon
        closable
        description={
          hasFilterData
            ? local.translate('client/filter/serverError')
            : local.translate('client/filter/noData')
        }
      />
      <br />
      <Button
        type={'primary'}
        onClick={() => getClientList()}
        icon={<ReloadOutlined />}
      >
        {local.translate('client/load/clientData')}
      </Button>
    </div>
  );
}

export default ClientsPage;
