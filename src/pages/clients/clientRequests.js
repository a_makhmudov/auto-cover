import clientService from '../../services/http/clientService';

export async function getClients() {
  const response = await clientService.getClients();
  return response.data;
}

export async function deleteClients(clientId) {
  const response = await clientService.deleteClients(clientId);
  return response.data;
}

export async function changeAutoCoverStatus(clientId) {
  return await clientService.changeAutoCoverStatus(clientId);
}

export async function getClientsByPages(pageNum) {
  const response = await clientService.getClientsByPage(pageNum);
  return response.data;
}
