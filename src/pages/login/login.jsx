import React from 'react';
import { Button, Form, Input, message } from 'antd';

import './login.scss';
import logo from '../../assets/images/anorbank-logo.png';
import authService from '../../services/http/authService';
import { useDispatch } from 'react-redux';
import { setUser } from '../../store/reducers/user';
import { useNavigate } from 'react-router-dom';
import { HOME_ROUTE } from '../../routing/appRouteConstants';

function LoginPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onFinish = (values) => {
    const { username, password } = values;
    authService
      .login({
        username,
        password,
      })
      .then((response) => {
        const data = response.data;
        if (data.access_token) {
          localStorage.setItem('isCorrectPassword', 'false');
          localStorage.setItem('login_data', JSON.stringify(data));
          localStorage.setItem('access_token', data.access_token);
          message.success('Log in successed!');
          dispatch(
            setUser({
              ...data,
              isAuth: true,
            })
          );
          navigate(`/${HOME_ROUTE}`);
        }
      })
      .catch((e) => message.error(e.message));
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className="login-page">
      <Form
        name="login"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <img style={{ width: '300px', margin: '0 0 30px 30px' }} src={logo} />
        <Form.Item
          label="Имя пользователя"
          name="username"
          rules={[
            {
              required: true,
              message: 'Пожалуйста, введите ваше имя пользователя!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Пароль"
          name="password"
          rules={[
            { required: true, message: 'Пожалуйста, введите свой пароль!' },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Войти
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default LoginPage;
