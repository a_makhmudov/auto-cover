import React, { useEffect } from 'react';
import { Card, Table, Typography } from 'antd';

import './clientDetailsPaymentDetails.scss';
import { clientPaymentColumns } from '../../../helpers/tableColumns';
import { useDispatch, useSelector } from 'react-redux';
import { getClientCoverHistory } from '../clientsDetailsRequest';
import { setClientPayments } from '../../../store/reducers/clientDetails';
import { useParams } from 'react-router-dom';

function ClientDetailsPaymentDetails() {
  const { clientId } = useParams();
  const dispatch = useDispatch();
  const clientPaymentDetails = useSelector(
    (state) => state.clientDetails.payments
  );

  useEffect(async () => {
    const paymentDetails = await getClientCoverHistory(clientId);
    dispatch(setClientPayments({ payments: paymentDetails }));
  }, []);

  const getPaymentTableTitle = () => (
    <Typography.Title level={3}>
      Детали по платежам: &nbsp; <span>{clientPaymentDetails.fio}</span>
    </Typography.Title>
  );
  return (
    <Card className="payment-details">
      <Table
        rowKey="pan"
        key={clientPaymentDetails.key}
        title={getPaymentTableTitle}
        scroll={{ x: 1400 }}
        columns={clientPaymentColumns}
        dataSource={clientPaymentDetails}
        pagination={false}
      />
    </Card>
  );
}

export default ClientDetailsPaymentDetails;
