import React, { useState } from 'react';

import './clientDetails.scss';
import ClientDetailsCardData from './clientDetailsCardData/clientDetailsCardData';
import ClientDetailsClientData from './clientDetailsClientData/clientDetailsClientData';
import ClientDetailsPaymentDetails from './clientDetailsPaymentDetails/clientDetailsPaymentDetails';
import { AddNewCard } from '../../components/addCard/addNewCard';
import { Modal } from 'antd';
import local from '../../helpers/local/index';

function ClientDetailsPage() {
  //-------------------- Modal settings  ------------------//

  const [isModalVisible, setIsModalVisible] = useState(false);
  const inputInvalidOptions = { panValid: false, expDateValid: false };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = (type) => {
    setIsModalVisible(type);
  };

  const handleCancel = (type) => {
    setIsModalVisible(type);
  };

  // -------------------- Modal settings ------------------ //

  return (
    <div className="client-details">
      <Modal
        closable={true}
        title={local.translate('client/card/add')}
        visible={isModalVisible}
        footer={null}
        onCancel={() => handleCancel(false)}
      >
        <AddNewCard handeCancel={handleCancel} handeOk={handleOk} />
      </Modal>
      <ClientDetailsClientData />
      <ClientDetailsCardData
        showModal={showModal}
        inputInvalidOptions={inputInvalidOptions}
      />
      <ClientDetailsPaymentDetails />
    </div>
  );
}

export default ClientDetailsPage;
