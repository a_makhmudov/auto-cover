import React, { useEffect, useState } from 'react';
import { Button, Card, Col, List, Row, Table, Typography } from 'antd';
import { CreditCardOutlined, DollarOutlined } from '@ant-design/icons';
import './clientDetailsClientData.scss';
import clientService from '../../../services/http/clientService';
import { useParams } from 'react-router-dom';
import { coverClient } from '../clientsDetailsRequest';
import { useDispatch, useSelector } from 'react-redux';
import { setClientDetails } from '../../../store/reducers/clientDetails';
import {
  clientDebitsClientDetails,
  clientDetailsClientData,
} from '../../../helpers/tableColumns';
import local from '../../../helpers/local';
import CurrencyFormat from 'react-currency-format';

function ClientDetailsClientData() {
  const { clientId } = useParams();
  const dispatch = useDispatch();
  const [allClientDebits, setAllClientDebits] = useState([]);
  const [allDebitListVisible, setAllDebitListVisible] = useState(false);

  useEffect(async () => {
    const response = await clientService.getClientDetailsByClientId(clientId);
    dispatch(setClientDetails({ details: response.data }));
    const allClientDebits = await clientService.getClientDebitByClientId(
      clientId
    );
    setAllClientDebits(allClientDebits.data);
  }, []);
  const clientDetails = useSelector((state) => state.clientDetails.details);
  return (
    <Card className="client-data">
      <Typography.Title level={3} style={{ paddingLeft: '30px' }}>
        {local.translate('client/clients')}
      </Typography.Title>
      <Row>
        <Col span={18}>
          <List
            bordered={true}
            dataSource={clientDetailsClientData}
            itemLayout="vertical"
            size="large"
            renderItem={(item) => {
              if (item.dataIndex === 'isAutoCover') {
                const status = clientDetails?.isAutoCover
                  ? local.translate('client/isAutoCover/on')
                  : local.translate('client/isAutoCover/off');
                return (
                  <List.Item>
                    <Row justify="space-between">
                      <Col span={9}>{item.title}</Col>
                      <Col span={9}>{status}</Col>
                    </Row>
                  </List.Item>
                );
              }
              if (item.dataIndex === 'totalDebit') {
                return (
                  <List.Item>
                    <Row justify="space-between">
                      <Col span={9}>{item.title}</Col>
                      <Col span={9}>
                        <CurrencyFormat
                          displayType={'text'}
                          value={clientDetails.totalDebit / 100}
                          thousandSeparator={' '}
                        />
                      </Col>
                    </Row>
                  </List.Item>
                );
              }
              return (
                <div>
                  <List.Item>
                    <Row justify="space-between">
                      <Col span={9}>{item.title}</Col>
                      <Col span={9}>
                        {clientDetails[item.dataIndex]?.toString()}
                      </Col>
                    </Row>
                  </List.Item>
                </div>
              );
            }}
          />
        </Col>
        <Col xs={{ span: 24 }} lg={{ span: 6 }}>
          <div className="buttons">
            <Button
              icon={<CreditCardOutlined />}
              style={{ backgroundColor: '#28A745' }}
              onClick={() => coverClient(clientId)}
            >
              {local.translate('client/payment')}
            </Button>
            <Button
              icon={<DollarOutlined />}
              style={{ backgroundColor: '#FA582E' }}
              onClick={() => setAllDebitListVisible(!allDebitListVisible)}
            >
              {local.translate('client/debts')}
            </Button>
          </div>
        </Col>
      </Row>
      {allDebitListVisible ? (
        <Row>
          <Col xs={{ span: 24 }} lg={{ span: 12 }}>
            <Card className="card-data">
              <Table
                rowKey={'creditId'}
                columns={clientDebitsClientDetails}
                dataSource={allClientDebits}
                pagination={false}
                scroll={{ y: 240 }}
              />
            </Card>
          </Col>
        </Row>
      ) : (
        ''
      )}
    </Card>
  );
}

export default ClientDetailsClientData;
