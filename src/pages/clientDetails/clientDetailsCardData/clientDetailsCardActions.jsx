import React, { useState } from 'react';
import { Button, message, Modal, Space } from 'antd';
import {
  coverClientCard,
  deleteClientCard,
  getClientCardsByClientId,
} from '../clientsDetailsRequest';
import { DeleteOutlined, ReloadOutlined } from '@ant-design/icons';
import { useParams } from 'react-router-dom';
import { setClientCards } from '../../../store/reducers/clientDetails';
import { useDispatch } from 'react-redux';
import local from '../../../helpers/local/';
import clientService from '../../../services/http/clientService';

export const ClientDetailsCardActions = ({ balance, pan, cardId }) => {
  const { clientId } = useParams();
  const dispatch = useDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const deleteClientsCard = async () => {
    const response = await deleteClientCard(cardId);
    if (response.data) {
      const clientCards = await getClientCardsByClientId(clientId);
      dispatch(setClientCards({ cards: clientCards }));
      message.success(local.translate('client/card/delete-success'));
    } else message.error(local.translate('client/card/delete-error'));
  };

  const updateBalance = async () => {
    const response = await clientService.updateCardBalance(pan);
    if (response.data) {
      message.success(local.translate('client/card/actions/success'));
      const clientCards = await getClientCardsByClientId(clientId);
      dispatch(setClientCards({ cards: clientCards }));
    } else {
      message.error(local.translate('client/card/actions/error'));
    }
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = async () => {
    await deleteClientsCard();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <Space>
      <Modal
        title="Вы действительно хотите удалить?"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      />
      <Button
        type="primary"
        onClick={() =>
          coverClientCard(clientId, {
            balance: balance,
            pan: pan,
          })
        }
      >
        {local.translate('client/card/cover')}
      </Button>

      <Button
        type="ghost"
        icon={<DeleteOutlined />}
        onClick={() => showModal()}
        danger
      />
      <Button
        type="primary"
        icon={<ReloadOutlined />}
        onClick={() => updateBalance(pan)}
      />
    </Space>
  );
};
