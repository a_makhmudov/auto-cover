import React, { useEffect, useState } from 'react';
import { Button, Card, Col, message, Row, Table, Typography } from 'antd';

import './clientDetailsCardData.scss';
import Spinner from '../../../components/spinner/spinner';
import { clientCardColumns } from '../../../helpers/tableColumns';
import { useDispatch, useSelector } from 'react-redux';
import { getClientCardsByClientId } from '../clientsDetailsRequest';
import { useParams } from 'react-router-dom';
import { setClientCards } from '../../../store/reducers/clientDetails';
import local from '../../../helpers/local';
import { RoleBasedUi } from '../../../components/role-based-ui/roleBasedUi';
import {
  ROLE_AUTOCOVER_ADMIN,
  ROLE_AUTOCOVER_USER,
} from '../../../helpers/roles/roles';
import clientService from '../../../services/http/clientService';

function ClientDetailsCardData({ showModal }) {
  const [loading, setLoading] = useState(false);
  const cards = useSelector((state) => state.clientDetails.cards);
  const [clientDetailsState, setClientDetailsState] = useState({});
  const [clientPinfl, setClientPinfl] = useState('');
  const { clientId } = useParams();
  const dispatch = useDispatch();
  useEffect(async () => {
    setLoading(true);
    const clientCards = await getClientCardsByClientId(clientId);
    const clientDetails = await clientService.getClientDetailsByClientId(
      clientId
    );
    setClientPinfl(clientDetails.data.pinfl);
    setClientDetailsState(clientDetails.data);
    dispatch(setClientCards({ cards: clientCards }));
    setLoading(false);
  }, []);
  if (loading) {
    return <Spinner />;
  }

  const getByPinfl = async () => {
    try {
      const response = await clientService.getByPinfl(clientId, clientPinfl);
      if (response?.data?.length > 0) {
        message.success(local.translate('client/card/actions/success'));
      } else message.error(local.translate('client/card/actions/error'));
    } catch (e) {
      console.log(e);
    }
  };

  const getByPassport = async () => {
    try {
      const response = await clientService.getCardByPassport(
        clientDetailsState.clientId,
        clientDetailsState.passportSerial,
        clientDetailsState.passportNumber
      );
      if (response?.data?.length > 0) {
        message.success(local.translate('client/card/actions/success'));
      } else message.error(local.translate('client/card/actions/error'));
    } catch (e) {
      console.log(e);
    }
  };

  const getCardTableTitle = () => (
    <Row justify="space-between" className="card-data-title">
      <Col span={18}>
        <Typography.Title level={3}>
          <strong>Карты:</strong> &nbsp; <span>{cards?.fio}</span>
        </Typography.Title>
      </Col>
      <Col span={24} style={{ textAlign: 'end' }}>
        <RoleBasedUi roles={[ROLE_AUTOCOVER_ADMIN]}>
          <Button
            type="link"
            className="card-data__buttons-pinfl"
            style={{ background: '#FFC107', borderColor: 'none !important' }}
            onClick={() => getByPassport()}
          >
            {local.translate('client/card/search/passport')}
          </Button>
        </RoleBasedUi>

        <RoleBasedUi roles={[ROLE_AUTOCOVER_ADMIN]}>
          <Button
            type="link"
            className="card-data__buttons-pinfl"
            style={{ backgroundColor: '#FA582E' }}
            onClick={() => getByPinfl()}
          >
            {local.translate('client/card/search/pinfl')}
          </Button>
        </RoleBasedUi>
        <RoleBasedUi roles={[ROLE_AUTOCOVER_ADMIN, ROLE_AUTOCOVER_USER]}>
          <Button
            style={{ backgroundColor: '#28A745' }}
            type="link"
            className="card-data__buttons-addCard"
            onClick={() => showModal(true)}
          >
            {local.translate('client/card/add')}
          </Button>
        </RoleBasedUi>
        {/*<Button*/}
        {/*  type="primary"*/}
        {/*className="card-data__buttons"*/}
        {/*  style={{ backgroundColor: '#3751FF', borderRadius: '4px' }}*/}
        {/*>*/}
        {/*  {local.translate('client/card/search')}*/}
        {/*</Button>*/}
      </Col>
    </Row>
  );

  return (
    <Card className="card-data">
      <Table
        rowKey="pan"
        title={getCardTableTitle}
        scroll={{ x: 1400 }}
        columns={clientCardColumns}
        dataSource={cards}
        pagination={false}
      />
    </Card>
  );
}

export default ClientDetailsCardData;
