import clientService from '../../services/http/clientService';
import { message } from 'antd';
import local from '../../helpers/local';

export async function getClientCardsByClientId(clientId) {
  try {
    const response = await clientService.getClientCardsByClientId(clientId);
    return response.data.content;
  } catch (e) {
    console.log(e);
  }
}

export async function coverClient(clientId) {
  const coverData = await clientService.coverClientAllDebits(clientId);
  switch (coverData.data) {
    case true:
      return message.success(local.translate('client/cover-success'));
    case false:
      return message.error(local.translate('client/cover-fail'));
    default:
      return message.info(local.translate('client/cover-on-hold'));
  }
}

export async function coverClientCard(clientId, body) {
  const coverCardData = await clientService.coverClientCard(clientId, body);
  switch (coverCardData.data) {
    case true:
      return message.success(local.translate('client/cover-success'));
    case false:
      return message.error(local.translate('client/cover-fail'));
    default:
      return message.info(local.translate('client/cover-on-hold'));
  }
}

export async function deleteClientCard(cardPan) {
  return await clientService.deleteClientCard(cardPan);
}

export async function getClientCoverHistory(clientId) {
  let clientCoverHistory = await clientService.getClientCoverHistory(clientId);
  return clientCoverHistory.data;
}

export async function getClientDebitsByClientID(clientId) {
  const response = await clientService.getClientDebitByClientId(clientId);
  return response.data;
}
