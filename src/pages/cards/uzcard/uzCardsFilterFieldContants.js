import { FILTER_FIELD_TYPES } from '../../../helpers/appConstants';

export const UzCardsFilterFieldContants = [
  {
    label: 'Номер карты',
    placeholder: '',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'pan',
    reqKey: 'pan',
  },
  {
    label: 'ФИО',
    placeholder: '',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'cardName',
    reqKey: 'cardName',
  },
  {
    label: 'ПИНФЛ',
    placeholder: '',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'pinfl',
    reqKey: 'pinfl',
  },

  {
    label: 'Серия паспорта',
    placeholder: '',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'passportSerial',
    reqKey: 'passportSerial',
  },
  {
    label: 'Номер паспорта',
    placeholder: '',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'passportNumber',
    reqKey: 'passportNumber',
  },
  {
    label: 'Дата рождения',
    placeholder: '',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'birthday',
    reqKey: 'birthday',
  },
  // {
  //   label: 'ID :',
  //   placeholder: 'Введите ID',
  //   type: FILTER_FIELD_TYPES.TEXT,
  //   defaultValue: '',
  //   dataIndex: 'id',
  //   reqKey: 'id',
  // },

  // {
  //   label: 'Срок карты',
  //   placeholder: '',
  //   type: FILTER_FIELD_TYPES.TEXT,
  //   defaultValue: '',
  //   dataIndex: 'expiry',
  //   reqKey: 'expiry',
  // },
  //
  // {
  //   label: 'Баланс',
  //   placeholder: '',
  //   type: FILTER_FIELD_TYPES.TEXT,
  //   defaultValue: '',
  //   dataIndex: 'balance',
  //   reqKey: 'balance',
  // },
];
