import React, { useEffect, useState } from 'react';
import DataTable from '../../../components/dataTable/dataTable';
import local from '../../../helpers/local';
import { cardsPageColumn } from '../../../helpers/tableColumns';
import {
  Alert,
  Button,
  Card,
  Col,
  Form,
  Input,
  message,
  Modal,
  Row,
  Table,
} from 'antd';
import cardsService from '../../../services/http/cardsService';
import { useDispatch, useSelector } from 'react-redux';
import {
  setCurrentPage,
  setImportUzcardList,
  setUzcard,
  setUzcardFilterData,
} from '../../../store/reducers/uzCard';
import { FilterFields } from '../../../components/filterFields/filterFields';
import Spinner from '../../../components/spinner/spinner';
import { UzCardsFilterFieldContants } from './uzCardsFilterFieldContants';
import { ReloadOutlined } from '@ant-design/icons';
import jwtDecode from 'jwt-decode';
import { ROLE_AUTOCOVER_ADMIN } from '../../../helpers/roles/roles';
import './uzcard.scss';

const CardUzcardPage = () => {
  const dispatch = useDispatch();
  const cardsList = useSelector((state) => state.uzCards);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]); // state for count of selected rows
  const [hasSelectedRow, setHasSelectedRow] = useState(false);
  const [isAdminRole, setIsAdminRole] = useState(false);
  const importCardsList = useSelector(
    (state) => state.uzCards.importUzcardList
  );
  const [passwordIsCorrect, setPasswordIsCorrect] = useState(false);
  const [checkPassword, setCheckPassword] = useState(false);
  const hasSelected = selectedRowKeys.length > 0; // this variable for disable  delete client button
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isFilterFieldModal, setFilterFieldModal] = useState(false);
  const uzCardFilterParams = useSelector((state) => state.uzCards.filterData);
  const [loading, setLoading] = useState(false);
  const currentPage = useSelector((state) => state.uzCards.currentPage);
  const [importFile, setImportFile] = useState(null);
  const [hasFilterData, setHasFiltersData] = useState(false);
  const localPasswordForUzcardPage = 'qwerty5131213';
  const [localPassword, setLocalPassword] = useState('');
  const [showLoadingCardsListResult, setShowLoadingCardsListResult] =
    useState(false);

  useEffect(async () => {
    let statusLocalPassword = localStorage.getItem('isCorrectPassword');
    statusLocalPassword === 'true'
      ? setPasswordIsCorrect(true)
      : setPasswordIsCorrect(false);
    await loadClientsListPerPage(currentPage);
    let jwt = localStorage.getItem('access_token');
    const decode_jwt = jwtDecode(jwt);
    if (decode_jwt.authorities.includes(ROLE_AUTOCOVER_ADMIN)) {
      setIsAdminRole(true);
    } else setIsAdminRole(false);
  }, [currentPage, uzCardFilterParams]);

  const loadClientsListPerPage = async (page) => {
    try {
      setLoading(true);
      const response = await cardsService.getAdditionalCardsList({
        ...uzCardFilterParams,
        page: page,
      });
      if (response.data.content.length <= 0) {
        message.error(local.translate('client/filter/noData'));
        dispatch(setUzcard([]));
        setHasFiltersData(true);
        setLoading(false);
        setFilterFieldModal(false);
      } else {
        setFilterFieldModal(false);
        setLoading(false);
        dispatch(setUzcard(response.data));
        message.success(local.translate('cards/uzcard/load/success'));
        setHasFiltersData(false);
      }
    } catch (e) {
      console.log(e);
    }
  };
  const filterData = async (filterData) => {
    const modifiedFilterData = {
      ...filterData,
      cardName: filterData?.cardName?.toUpperCase(),
    };
    dispatch(setUzcardFilterData(modifiedFilterData));
    dispatch(setCurrentPage(0));
  };
  const importUzCardList = async () => {
    try {
      const response = await cardsService.importAdditionCardsList(importFile);
      if (response?.data !== {}) {
        dispatch(setImportUzcardList(response.data));
        setIsModalVisible(false);
        setShowLoadingCardsListResult(true);
      } else {
        message.error(local.translate('anor/pay/loadFile/error'));
      }
    } catch (e) {
      message.error(e.message);
    }
  };
  const getUzCardList = async () => {
    dispatch(setUzcardFilterData({}));
    dispatch(setCurrentPage(0));
  };

  const deleteUzcards = async (clientsId) => {
    const ids = clientsId.map((item) => {
      return item;
    });
    const deleteResponse = await cardsService.deleteUzcards(ids);
    if (deleteResponse.data) {
      setHasSelectedRow(true);
      setSelectedRowKeys([]);
      await loadClientsListPerPage(currentPage);
      message.success(local.translate('cards/uzcard/delete/success'));
      setHasSelectedRow(true);
    } else {
      message.error(local.translate('cards/uzcard/delete/error'));
      setHasSelectedRow(false);
    }
  };

  const confirmPassword = () => {
    if (localPassword === localPasswordForUzcardPage) {
      localStorage.removeItem('isCorrectPassword');
      localStorage.setItem('isCorrectPassword', 'true');
      setPasswordIsCorrect(true);
      setCheckPassword(false);
      setTimeout(() => {
        localStorage.removeItem('isCorrectPassword');
        localStorage.setItem('isCorrectPassword', 'false');
        setPasswordIsCorrect(false);
      }, 3600000);
    } else {
      setCheckPassword(true);
      setPasswordIsCorrect(false);
      localStorage.removeItem('isCorrectPassword');
      localStorage.setItem('isCorrectPassword', 'false');
    }
  };

  //------------------- This function will call when select one or more rows   -----------------//
  const onSelectChange = (selectedRowKeys) => {
    setSelectedRowKeys(selectedRowKeys);
  };
  //------------------- This function will call when select one or more rows -----------------//

  //---------------------- select all or invert all data setting -----------------//
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [Table.SELECTION_ALL, Table.SELECTION_NONE],
  };
  //---------------------- select all or invert all data setting -----------------//

  if (loading) {
    return <Spinner />;
  }

  if (passwordIsCorrect) {
    return cardsList?.content?.length > 0 ? (
      <Card className="payments">
        <Modal
          className="payments__result-loading-cards-list"
          centered={true}
          footer={null}
          title={'Результат загрузки карты'}
          visible={showLoadingCardsListResult}
          onOk={() => setShowLoadingCardsListResult(false)}
          onCancel={() => setShowLoadingCardsListResult(false)}
        >
          <p>
            Общая количество загружаемых данных:
            <strong>&nbsp; {importCardsList.countTotal}</strong>
          </p>
          <p>
            - Добавлено:<strong>&nbsp; {importCardsList.countAdded}</strong>
          </p>
          <p>
            - Обновлено:<strong>&nbsp; {importCardsList.countUpdated}</strong>
          </p>
          <Button
            onClick={() => setShowLoadingCardsListResult(false)}
            type={'primary'}
          >
            OK
          </Button>
        </Modal>

        <Modal
          title={local.translate('client/filter')}
          visible={isFilterFieldModal}
          onOk={() => filterData(uzCardFilterParams)}
          onCancel={() => setFilterFieldModal(false)}
          footer={null}
        >
          <FilterFields
            defaultParams={uzCardFilterParams}
            fields={UzCardsFilterFieldContants}
            filter={filterData}
            closeFilterModal={() => setFilterFieldModal(false)}
          />
        </Modal>

        <Modal
          title={local.translate('client/import/data')}
          visible={isModalVisible}
          onOk={() => {}}
          onCancel={() => setIsModalVisible(false)}
          footer={null}
        >
          <Form>
            <Form.Item name="file">
              <Input
                type="file"
                name="file"
                onChange={(e) => {
                  setImportFile(e.target.files[0]);
                }}
              />
            </Form.Item>
            <Row justify="end">
              <Col span={5} offset={13}>
                <Button
                  type="ghost"
                  size="large"
                  onClick={() => setIsModalVisible(false)}
                >
                  {local.translate('client/import/cancel')}
                </Button>
              </Col>
              <Col span={5} offset={1}>
                <Button
                  htmlType="submit"
                  type="primary"
                  size="large"
                  onClick={() => importUzCardList()}
                >
                  {local.translate('client/import/ok')}
                </Button>
              </Col>
            </Row>
          </Form>
        </Modal>
        <DataTable
          hasSelected={!hasSelected}
          buttonLabel={local.translate('anor/pay/download')}
          rowKey="id"
          columns={cardsPageColumn}
          data={cardsList?.content || null}
          hasSelectedRow={hasSelectedRow}
          rowSelection={isAdminRole ? rowSelection : null}
          title={
            <>
              <strong>Количество: </strong>
              &nbsp;
              {cardsList.totalElements}
            </>
          }
          clearBtnClickHandler={() => getUzCardList()}
          importBtnClickHandler={() => setIsModalVisible(true)}
          filterBtnClickHandler={() => setFilterFieldModal(true)}
          deleteBtnClickHandler={
            isAdminRole ? () => deleteUzcards(selectedRowKeys) : null
          }
          pagination={{
            current: currentPage + 1,
            pageSize: 20,
            total: cardsList.totalElements || null,
            showSizeChanger: false,
            showLessItems: true,
            showQuickJumper: {
              goButton: (
                <Button>
                  {local.translate('anor/pay/table/pagination/jumper')}
                </Button>
              ),
            },
            onChange: (page) => {
              dispatch(setCurrentPage(page - 1));
            },
          }}
        />
      </Card>
    ) : (
      <div className="no-data">
        <Alert
          message={local.translate('client/alert/warning')}
          type="warning"
          showIcon
          closable
          description={
            hasFilterData
              ? local.translate('client/filter/serverError')
              : local.translate('client/filter/noData')
          }
        />
        <br />
        <Button
          type={'primary'}
          onClick={() => getUzCardList()}
          icon={<ReloadOutlined />}
        >
          {local.translate('cards/load/cardsList')}
        </Button>
      </div>
    );
  } else {
    return (
      <div className="payments__confirmation-password">
        <form
          className="payments__confirmation-password__form"
          autoComplete="off"
          initialValues={{
            remember: false,
          }}
        >
          <label
            className="payments__confirmation-password__form-label"
            htmlFor="password"
          >
            Введите пароль
          </label>
          <Form.Item
            rules={[
              {
                required: true,
                message: 'Пожалуйста введите пароль',
              },
            ]}
          >
            <Input.Password
              type="text"
              id="passwordForUzcard"
              onChange={(e) => setLocalPassword(e.target.value)}
              autoComplete={'off'}
            />
            {checkPassword ? (
              <span style={{ color: 'red' }}>Неверный пароль</span>
            ) : (
              ''
            )}
          </Form.Item>

          <Button
            className="payments__confirmation-password__form-submit"
            type={'primary'}
            onClick={confirmPassword}
            disabled={localPassword !== '' ? false : true}
          >
            Войти
          </Button>
        </form>
      </div>
    );
  }
};

export default CardUzcardPage;
