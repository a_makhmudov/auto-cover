import React from 'react';
import { Card } from 'antd';
import DataTable from '../../components/dataTable/dataTable';

import './terminal.scss';

function TerminalsPage() {
  const columns = [
    {
      title: '№',
      dataIndex: 'id',
    },
    {
      title: 'Название терминала',
      dataIndex: 'terminalName',
    },
    {
      title: 'ID Терминала',
      dataIndex: 'terminalId',
    },
    {
      title: 'ID Пункт обслуживания',
      dataIndex: 'servicePointId',
    },
    {
      title: 'Описание',
      dataIndex: 'description',
    },
    {
      title: 'Активность',
      dataIndex: 'activity',
    },
  ];

  const data = [
    {
      key: '1',
      id: '1',
      terminalName: '860023456789',
      terminalId: '1234567',
      servicePointId: '7654321',
      description: 'some description',
      activity: '435 300',
    },
    {
      key: '2',
      id: '2',
      terminalName: '860023456789',
      terminalId: '1234567',
      servicePointId: '7654321',
      description: 'some description',
      activity: '435 300',
    },
  ];

  return (
    <Card className="terminals">
      <DataTable
        rowKey={'id'}
        columns={columns}
        data={data}
        title={'Терминалы'}
      />
    </Card>
  );
}

export default TerminalsPage;
