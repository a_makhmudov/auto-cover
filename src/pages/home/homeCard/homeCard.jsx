import React, { useEffect, useState } from 'react';
import { Button, Card, Row } from 'antd';
import CurrencyFormat from 'react-currency-format';
import {
  DollarOutlined,
  LineChartOutlined,
  PoundOutlined,
  RightOutlined,
  UserOutlined,
} from '@ant-design/icons';
import './homeCard.scss';
import clientService from '../../../services/http/clientService';
import debitsService from '../../../services/http/debitsService';
import paymentService from '../../../services/http/paymentService';

function HomeCard() {
  const [totalData, setTotalData] = useState({
    clients: null,
    debits: null,
    payment: null,
    residue: null,
  });
  useEffect(async () => {
    const totalClientCount = await clientService.getClients({});
    const totalDebit = await debitsService.getTotalDebitSum();
    const totalPayment = await paymentService.getAllPaymentsSum();
    const totalResidue = await paymentService.getAllResidueSum();
    setTotalData({
      clients: totalClientCount.data.totalElements,
      debits: totalDebit.data,
      payment: totalPayment.data,
      residue: totalResidue.data,
    });
  }, []);

  const cardsData = [
    {
      text: 'Всего клиентов',
      dataIndex: 'allClients',
      link: 'empty-link',
      icon: <UserOutlined style={{ color: '#29CC97' }} />,
      amount: totalData.clients,
      iconBgColor: 'rgba(41, 204, 151, 0.15)',
    },
    {
      text: 'Обшая сумма долга',
      dataIndex: 'allDebits',
      link: 'empty-link',
      icon: <LineChartOutlined style={{ color: '#FFC107' }} />,
      amount: totalData.debits / 100,
      iconBgColor: 'rgba(255, 193, 7, 0.15)',
    },
    {
      text: 'Обшая оставшаяся сумма',
      dataIndex: 'allResidue',
      link: 'empty-link',
      icon: <DollarOutlined style={{ color: '#FA582E' }} />,
      amount: totalData.residue / 100,
      iconBgColor: 'rgba(250, 88, 46, 0.15)',
    },
    {
      text: 'Общая оплаченная сумма',
      dataIndex: 'allPayments',
      link: 'empty-link',
      icon: <PoundOutlined style={{ color: '#3751FF' }} />,
      amount: totalData.payment / 100,
      iconBgColor: 'rgba(55, 81, 255, 0.15)',
    },
  ];

  return cardsData.map((card, key) => (
    <Card className="card" hoverable key={key}>
      <Row align="middle" wrap={false}>
        <div
          className="icon-wrapper"
          style={{ backgroundColor: card.iconBgColor }}
        >
          {card.icon}
        </div>
        <div className="text">{card.text}</div>
      </Row>
      <Row align="middle" justify="space-between">
        <div className="amount">
          {card.dataIndex === 'allClients' ? (
            card.amount
          ) : (
            <CurrencyFormat
              displayType={'text'}
              value={card.amount}
              thousandSeparator={' '}
            />
          )}
        </div>
        <Button
          className="btn-next"
          type="primary"
          icon={<RightOutlined className="icon-next" />}
        />
      </Row>
    </Card>
  ));
}

export default HomeCard;
