import { Row, Switch, Table } from 'antd';
import React from 'react';

import './taskTable.scss';

function TaskTable() {
  const columns = [
    {
      title: '№',
      dataIndex: 'id',
    },
    {
      title: 'Задачи',
      dataIndex: 'tasks',
    },
    {
      title: 'Cтатус',
      dataIndex: 'status',
    },
    {
      title: 'Время последнего обновления',
      dataIndex: 'lastUpdate',
    },
  ];
  const data = [
    {
      key: '1',
      id: '1',
      tasks: 'Автосписание',
      status: (
        <div>
          <Switch className="inactive" defaultChecked={false} /> &nbsp; Не
          активен
        </div>
      ),
      lastUpdate: '16.09.2021  17:30',
    },
    {
      key: '2',
      id: '2',
      tasks: 'Автосписание',
      status: (
        <div>
          <Switch className="active" defaultChecked={true} /> &nbsp; Активен
        </div>
      ),
      lastUpdate: '23.09.2021  16:00',
    },
  ];
  return (
    <Row>
      <Table
        className="task-table"
        columns={columns}
        dataSource={data}
        pagination={false}
        title={() => <span>Управление задачами</span>}
      />
    </Row>
  );
}

export default TaskTable;
