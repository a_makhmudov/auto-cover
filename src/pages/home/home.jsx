import React from 'react';
import { Layout, Row } from 'antd';
import './home.scss';
import HomeCard from './homeCard/homeCard';
import StatsByCards from './statsByCards/statsByCards';
import StatsByCardsCircle from './statsByCardsCircle/statsByCardsCircle';
import ActionButtons from './actionButtons/actionButtons';
// import TaskTable from './taskTable/taskTable';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { LOGIN_ROUTE } from '../../routing/appRouteConstants';

function HomePage() {
  const user = useSelector((state) => state.user);

  const navigate = useNavigate();

  const login_data = JSON.parse(localStorage.getItem('login_data'));

  if (!user.isAuth && !login_data?.access_token) {
    navigate(`/${LOGIN_ROUTE}`);
  }

  return (
    <Layout className="home">
      <Row
        className="cards"
        justify="space-between"
        align="middle"
        gutter={[20, 20]}
      >
        {<HomeCard />}
      </Row>
      <StatsByCards />
      <StatsByCardsCircle />
      <ActionButtons />
      {/*<TaskTable />*/}
    </Layout>
  );
}

export default HomePage;
