import React, { useEffect, useState } from 'react';
import { Card, Col, DatePicker, Row, Typography } from 'antd';
import { Pie } from '@ant-design/charts';
import './statsByCardsCircle.scss';
import coverService from '../../../services/http/coverService';
import moment from 'moment';
import { Currency } from '../../../components/mask/currency';
import { numberWithCommas } from '../statsByCards/amountFormatter';

function StatsByCardsCircle() {
  const [coverListByDay, setCoverListByDay] = useState({});
  const [coverListByMonth, setCoverListByMonth] = useState({});
  const [currentDateDay] = useState(moment());
  const [currentDateMonth] = useState(moment());
  useEffect(async () => {
    const coverListByDayInner = await coverService.getCoverListByDay(
      currentDateDay.format('YYYY-MM-DD')
    );
    const coverListByMonthInner = await coverService.getCoverListByMonth(
      currentDateMonth.format('YYYY-MM') + '-01'
    );
    setCoverListByDay(coverListByDayInner?.data);
    setCoverListByMonth(coverListByMonthInner?.data);
  }, []);
  const getCoverListByDay = async (date, dateString) => {
    const coverListByDay = await coverService.getCoverListByDay(dateString);
    setCoverListByDay(coverListByDay.data);
  };

  const getCoverListByMonth = async (date, dateString) => {
    let firstDayOfMonth = `${dateString}-01`;
    const coverListByMonth = await coverService.getCoverListByMonth(
      firstDayOfMonth
    );
    setCoverListByMonth(coverListByMonth.data);
  };
  const configDayPieChart = {
    appendPadding: 10,
    data: coverListByDay?.sums || [],
    angleField: 'sums',
    colorField: 'type',
    radius: 0.6,
    legend: {
      flipPage: true,
      layout: 'vertical',
      position: 'bottom',
      maxWidth: 1000,
      itemValue: {
        formatter: (_, item) => {
          const result = coverListByDay.sums.find((i) => i.type === item.value);
          return `-> ${numberWithCommas(result.sums / 100)}`;
        },
      },
    },
    label: {
      type: 'spider',
      labelHeight: 2,
      content: '{name}\n{percentage}',
    },
    tooltip: {
      formatter: (data) => {
        return {
          name: data.type,
          value: numberWithCommas(data.sums / 100),
        };
      },
    },
    interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
  };

  const configMonthPieChart = {
    appendPadding: 10,
    data: coverListByMonth.sums || [],
    angleField: 'sums',
    colorField: 'type',
    radius: 0.6,
    legend: {
      flipPage: true,
      layout: 'vertical',
      position: 'bottom',
      maxWidth: 1000,
      itemValue: {
        formatter: (_, item) => {
          const result = coverListByMonth.sums.find(
            (i) => i.type === item.value
          );
          return `-> ${numberWithCommas(result.sums / 100)}`;
        },
      },
    },
    label: {
      type: 'spider',
      labelHeight: 28,
      content: '{name}\n{percentage}',
    },
    tooltip: {
      formatter: (data) => {
        return {
          name: data.type,
          value: numberWithCommas(data.sums / 100),
        };
      },
    },

    interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
  };
  return (
    <Row className="stats-by-cards-circle" justify="space-between" wrap={true}>
      <Col xs={{ span: 22 }} lg={{ span: 11 }}>
        <Card>
          <Typography.Title level={5}>
            Общая сумма транзакции за день: &nbsp;
            <Currency number={coverListByDay.allSums || 0} />
            <br />
            <br />
            <div
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <span>Выберите дату: &nbsp;</span>
              <DatePicker
                placeholder={'Выберите дату'}
                style={{ width: '200px' }}
                format={'YYYY-MM-DD'}
                defaultValue={currentDateDay}
                onChange={(date, dateString) =>
                  getCoverListByDay(date, dateString)
                }
              />
            </div>
          </Typography.Title>
          {Object.keys(coverListByDay).length === 0 ? (
            <div className="stats-by-cards-circle__no-data-transactions">
              Для текущей даты не имеется транзакции
            </div>
          ) : (
            <Pie {...configDayPieChart} />
          )}
        </Card>
      </Col>
      <Col xs={{ span: 22 }} lg={{ span: 11 }}>
        <Card>
          <Typography.Title level={5}>
            Общая сумма транзакции за месяц: &nbsp;
            <Currency number={coverListByMonth.allSums || 0} />
            <br />
            <br />
            <div
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <span>Выберите дату: &nbsp;</span>
              <DatePicker
                picker="month"
                style={{ width: '200px' }}
                format={'YYYY-MM'}
                defaultValue={currentDateMonth}
                placeholder={'Выберите месяц'}
                onChange={(date, dateStrings) =>
                  getCoverListByMonth(date, dateStrings)
                }
              />
            </div>
          </Typography.Title>
          {Object.keys(coverListByMonth).length === 0 ? (
            <div className="stats-by-cards-circle__no-data-transactions">
              Для текущей даты не имеется транзакции
            </div>
          ) : (
            <Pie {...configMonthPieChart} />
          )}
        </Card>
      </Col>
    </Row>
  );
}

export default StatsByCardsCircle;
