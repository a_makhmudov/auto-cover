import coverService from '../../../services/http/coverService';
import { message } from 'antd';
import local from '../../../helpers/local';
import taskSchedulingService from '../../../services/http/taskSchedulingService';

export const run_cover = async () => {
  try {
    const response = await coverService.coversRun();
    if (response.data) {
      message.success(local.translate('cover/run/success'));
    } else {
      message.error(local.translate('cover/run/error'));
    }
  } catch (e) {
    message.error(e.message);
  }
};

export const getAllSchedulingStatus = async () => {
  const response = await taskSchedulingService.getAllScheduleStatus();
  return response.data;
};
