import React, { useEffect, useState } from 'react';
import { Button, message, Row } from 'antd';
import './actionButtons.scss';
import local from '../../../helpers/local';
import { PlayCircleOutlined, StopOutlined } from '@ant-design/icons';
import { RoleBasedUi } from '../../../components/role-based-ui/roleBasedUi';
import { ROLE_AUTOCOVER_ADMIN } from '../../../helpers/roles/roles';
import coverService from '../../../services/http/coverService';

function ActionButtons() {
  const [runButtonDisable, setRunButtonDisable] = useState(false);
  const [stopButtonDisable, setStopButtonDisable] = useState(false);
  const [coverStatus, setCoverStatus] = useState(false);

  useEffect(async () => {
    const responseCoverStatus = await coverService.checkCoverStatus();
    setCoverStatus(responseCoverStatus.data);
    if (responseCoverStatus.data === true) {
      setRunButtonDisable(true);
    }
    if (responseCoverStatus.data === false) {
      setStopButtonDisable(true);
    }
  }, []);

  const startCover = async () => {
    try {
      const response = await coverService.coverStartStop();
      setCoverStatus(response.data);
      if (response.data === true) {
        setRunButtonDisable(true);
        setStopButtonDisable(false);
        message.success(local.translate('cover/run/success'));
      } else {
        message.error('cover/run/error');
        setRunButtonDisable(false);
      }
    } catch (e) {
      message.success(local.translate(e.message));
    }
  };
  const stopCover = async () => {
    try {
      const response = await coverService.coverStartStop();
      setCoverStatus(response.data);
      if (response.data === false) {
        setRunButtonDisable(false);
        setStopButtonDisable(true);
        message.success(local.translate('cover/stop/success'));
      } else {
        message.error('cover/stop/error');
      }
    } catch (e) {
      message.success(local.translate(e.message));
    }
  };

  return (
    <RoleBasedUi roles={[ROLE_AUTOCOVER_ADMIN]}>
      <Row className="stats-by-cards-circle">
        <div className="section-cover">
          <h2>
            Статус автосписания: &nbsp;
            {coverStatus ? (
              <span className="section-cover__cover-status_active">
                {local.translate('cover/status/active')}
              </span>
            ) : (
              <span className="section-cover__cover-status_inactive">
                {local.translate('cover/status/inactive')}
              </span>
            )}
          </h2>
          <div className="section-cover__action-buttons">
            <Button
              className="section-cover__run-operation button"
              onClick={() => startCover()}
              type={'primary'}
              danger={true}
              style={{ background: '#28a745' }}
              loading={runButtonDisable}
            >
              {local.translate('cover/run')}
              <PlayCircleOutlined />
            </Button>
            <Button
              className="section-cover__stop-operation button"
              onClick={() => stopCover()}
              type={'primary'}
              loading={stopButtonDisable}
            >
              {local.translate('cover/stop')}
              <StopOutlined />
            </Button>
          </div>
        </div>
      </Row>
    </RoleBasedUi>
  );
}

export default ActionButtons;
