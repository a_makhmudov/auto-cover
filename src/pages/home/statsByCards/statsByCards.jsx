import React, { useEffect, useState } from 'react';
import { Card, Col, Row, Typography } from 'antd';
import { Line } from '@ant-design/charts';
import local from '../../../helpers/local';
import './statsByCards.scss';
import { Currency } from '../../../components/mask/currency';
import coverService from '../../../services/http/coverService';
import { formatterAmount, numberWithCommas } from './amountFormatter';

function StatsByCards() {
  const [coverSumHalfYear, setCoverSumHalfYear] = useState({});
  useEffect(async () => {
    const coverListHalfYear = await coverService.getCoverListByHalfYear();
    setCoverSumHalfYear(coverListHalfYear?.data);
  }, []);
  const { allHumoSums, allManualSums, allSums, allUzCardSums } =
    coverSumHalfYear;

  const DateMaskDiagram = (date) => {
    let newData = new Date(date) || '';
    let year;
    let month;
    let day;
    if (newData) {
      year = newData.getFullYear().toString();
      month = newData.getMonth() + 1;
      day = newData.getDate();
    }

    if (month < 10) {
      const fullMonth = `0${month}`;
      month = fullMonth;
    }
    if (day < 10) {
      let fullDay = `0${day}`;
      day = fullDay;
    }

    return `${year}-${month}`;
  };

  const configOfCoverHalfYear = {
    data: coverSumHalfYear.coverByHistoryByHalfYear || [],
    xField: 'monthly',
    yField: 'sums',
    seriesField: 'type',
    tooltip: {
      formatter: (data) => {
        let sum = data.sums / 100;
        return {
          title: DateMaskDiagram(data.monthly),
          name: data.type,
          value: numberWithCommas(sum),
        };
      },
    },
    xAxis: {
      label: {
        formatter: function (v) {
          return DateMaskDiagram(v);
        },
      },
    },
    yAxis: {
      label: {
        formatter: function (v) {
          return formatterAmount(v);
        },
      },
    },
    legend: {
      layout: 'horizontal',
      position: 'top-right',
    },
    smooth: true,
    animation: {
      appear: {
        animation: 'path-in',
        duration: 2001,
      },
    },
  };
  return (
    <Row className="stats-by-cards">
      <Col md={{ span: 20 }}>
        <Card>
          <Typography.Title level={5}>
            {local.translate('controlPanel/statistics/card')}
          </Typography.Title>
          <Line {...configOfCoverHalfYear} />
        </Card>
      </Col>
      <Col md={{ span: 4 }}>
        <div className="stats-by-type">
          <Typography.Text className="stat-label">
            {local.translate('controlPanel/statistics/all')}
          </Typography.Text>
          <Typography.Title level={5} className="stat-value">
            {<Currency number={allSums} /> || 0}
          </Typography.Title>
        </div>
        <div className="stats-by-type">
          <Typography.Text className="stat-label">
            {local.translate('controlPanel/statistics/humo')}
          </Typography.Text>
          <Typography.Title level={5} className="stat-value">
            {<Currency number={allHumoSums} /> || 0}
          </Typography.Title>
        </div>
        <div className="stats-by-type">
          <Typography.Text className="stat-label">
            {local.translate('controlPanel/statistics/uzcard')}
          </Typography.Text>
          <Typography.Title level={5} className="stat-value">
            {<Currency number={allUzCardSums} /> || 0}
          </Typography.Title>
        </div>
        <div className="stats-by-type">
          <Typography.Text className="stat-label">
            {local.translate('controlPanel/statistics/handle')}
          </Typography.Text>
          <Typography.Title level={5} className="stat-value">
            {<Currency number={allManualSums} /> || 0}
          </Typography.Title>
        </div>
      </Col>
    </Row>
  );
}

export default StatsByCards;
