export const formatterAmount = (v) => {
  let amount = Number(v) / 100;
  let int = String(amount);
  if (int.length <= 3) return int;
  let space = 0;
  let number = '';

  for (let i = int.length - 1; i >= 0; i--) {
    if (space == 3) {
      number = ' ' + number;
      space = 0;
    }
    number = int.charAt(i) + number;
    space++;
  }
  return number;
};

export function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
