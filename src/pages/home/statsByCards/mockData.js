const data = [
  {
    month: 'Январь',
    payments: 10000000,
    name: 'Humo',
  },
  {
    month: 'Февраль',
    payments: 40000000,
    name: 'Humo',
  },
  {
    month: 'Март',
    payments: 34000000,
    name: 'Humo',
  },
  {
    month: 'Апрель',
    payments: 23000000,
    name: 'Humo',
  },
  {
    month: 'Май',
    payments: 75000000,
    name: 'Humo',
  },
  {
    month: 'Июнь',
    payments: 51000000,
    name: 'Humo',
  },

  {
    month: 'Январь',
    payments: 45000000,
    name: 'Uzcard',
  },
  {
    month: 'Февраль',
    payments: 40000000,
    name: 'Uzcard',
  },
  {
    month: 'Март',
    payments: 56000000,
    name: 'Uzcard',
  },
  {
    month: 'Апрель',
    payments: 34000000,
    name: 'Uzcard',
  },
  {
    month: 'Май',
    payments: 68000000,
    name: 'Uzcard',
  },
  {
    month: 'Июнь',
    payments: 59000000,
    name: 'Uzcard',
  },
  {
    month: 'Июль',
    payments: 15000000,
    name: 'Ручной',
  },
  {
    month: 'Август',
    payments: 8000000,
    name: 'Ручной',
  },
  {
    month: 'Сентябрь',
    payments: 20000000,
    name: 'Ручной',
  },
  {
    month: 'Октябрь',
    payments: 12000000,
    name: 'Ручной',
  },
  {
    month: 'Ноябрь',
    payments: 11000000,
    name: 'Ручной',
  },
  {
    month: 'Декабрь',
    payments: 17000000,
    name: 'Ручной',
  },
];

export default data;
