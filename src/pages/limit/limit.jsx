import React, { useEffect, useState } from 'react';
import { Card, Input, Form, Row, Col, Button, message } from 'antd';
import local from '../../helpers/local';
import './limit.scss';
import Space from 'antd/es/space';
import { getLimit } from './limitRequests';
import { useDispatch, useSelector } from 'react-redux';
import { setLimits } from '../../store/reducers/limit';
import limitService from '../../services/http/limitService';

function LimitPage() {
  const [disable, setDisable] = useState({
    limitSum: true,
    clientCountLimit: true,
    saveButton: true,
    changeButton: false,
  });
  const dispatch = useDispatch();

  const loadLimits = async () => {
    try {
      const response = await getLimit();
      dispatch(setLimits(response.data));
    } catch (e) {
      message.error(local.translate('limit/serverError'));
    }
  };

  useEffect(async () => {
    try {
      await loadLimits();
    } catch (e) {
      console.log(e);
    }
  }, []);

  const limitsData = useSelector((state) => state.limits);
  const { clientCountLimit, limitSum } = limitsData;
  const createOrUpdateLimits = async () => {
    try {
      const dataResponse = await limitService.createOrUpdateLimit(
        clientCountLimit,
        limitSum
      );
      setDisable({
        clientCountLimit: true,
        limitSum: true,
        saveButton: true,
        changeButton: false,
      });
      dispatch(setLimits(dataResponse.data));
      message.success(local.translate('limit/set/success'));
    } catch (e) {
      message.error(local.translate('limit/set/error'));
    }
  };

  const addSumLimit = (e) => {
    dispatch(setLimits({ ...limitsData, limitSum: e.target.value }));
  };
  const addClientCountLimit = (e) => {
    dispatch(setLimits({ ...limitsData, clientCountLimit: e.target.value }));
  };

  const setEnableOnChange = () => {
    setDisable({
      saveButton: false,
      limitSum: false,
      clientCountLimit: false,
      changeButton: true,
    });
  };

  return (
    <Row>
      <Col xs={{ span: 8 }}>
        <Card className="limit">
          <Form
            size={'small'}
            name="limit"
            colon={false}
            className="limit-form"
          >
            <Row justify="start">
              <Space direction={'vertical'} style={{ width: '100%' }}>
                <Form.Item
                  label={local.translate('limit/setLimit')}
                  name="installedLimit"
                >
                  <Input
                    type="text"
                    size="large"
                    onChange={(e) => addSumLimit(e)}
                    value={limitSum}
                    placeholder={limitSum}
                    disabled={disable.limitSum}
                  />
                </Form.Item>
                <Form.Item
                  label={local.translate('limit/numberWriteOffs')}
                  name="countOfPerPayments"
                >
                  <Input
                    type="text"
                    size="large"
                    onChange={(e) => addClientCountLimit(e)}
                    value={clientCountLimit}
                    placeholder={clientCountLimit}
                    disabled={disable.clientCountLimit}
                  />
                </Form.Item>
              </Space>
            </Row>
            <Row justify="end">
              <Space direction={'horizontal'}>
                <Col>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="submit-btn"
                    disabled={disable.changeButton}
                    onClick={() => setEnableOnChange()}
                  >
                    {local.translate('limit/change-button')}
                  </Button>
                </Col>
                <Col>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="submit-btn"
                    onClick={() => createOrUpdateLimits()}
                    disabled={disable.saveButton}
                  >
                    {local.translate('limit/save')}
                  </Button>
                </Col>
              </Space>
            </Row>
          </Form>
        </Card>
      </Col>
    </Row>
  );
}

export default LimitPage;
