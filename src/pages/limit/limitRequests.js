import limitService from '../../services/http/limitService';

export async function getLimit() {
  const limitsData = await limitService.getLimits();
  return limitsData;
}
