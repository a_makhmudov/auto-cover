import React from 'react';
import cronParser from 'cron-parser';

const addZero = (value) => {
  return value < 10 ? `0${value}` : value;
};

export const CronParser = ({ props }) => {
  const date = cronParser.parseExpression(props).next()._date.c;
  return (
    <div>{`${addZero(date.day)}.${addZero(date.month)}.${date.year} ${addZero(
      date.hour
    )}:${addZero(date.minute)}`}</div>
  );
};
