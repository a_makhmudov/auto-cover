import React, { useEffect, useState } from 'react';
import { Alert, Button, Col, message, Row, Table } from 'antd';
import { dataSchedulingColumns } from '../../helpers/tableColumns';
import './tasksScheduling.scss';
import { useDispatch, useSelector } from 'react-redux';
import { setScheduling } from '../../store/reducers/taskScheduling';
import local from '../../helpers/local';
import { ReloadOutlined } from '@ant-design/icons';
import taskSchedulingService from '../../services/http/taskSchedulingService';
import Spinner from '../../components/spinner/spinner';

function TaskSchedulingPage() {
  const dispatch = useDispatch();
  const taskScheduler = useSelector((state) => state.taskScheduler);
  const [loading, setLoading] = useState(false);
  useEffect(async () => {
    await loadTasks();
  }, []);

  const loadTasks = async () => {
    try {
      const response = await taskSchedulingService.getTasksList();
      dispatch(setScheduling(response.data));
      if (response.data.length !== 0) {
        message.success(local.translate('taskScheduling/getData/success'));
      }
      if (response.data.length === 0) {
        message.error(local.translate('taskScheduling/getData/error'));
      }
    } catch (e) {
      message.error(local.translate('taskScheduling/getData/error'));
    }
  };
  const resetTasks = async () => {
    try {
      setLoading(true);
      const response = await taskSchedulingService.resetTasksList();
      if (response.data.length !== 0) {
        message.success(local.translate('taskScheduling/getData/success'));
      }
      setLoading(false);
      if (response.data.length === 0) {
        message.error(local.translate('taskScheduling/getData/error'));
      }
    } catch (e) {
      message.error(local.translate('taskScheduling/getData/error'));
    }
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <div>
      {taskScheduler.data.length > 0 ? (
        <Table
          dataSource={taskScheduler.data}
          columns={dataSchedulingColumns}
          rowKey="id"
          className="data-table"
          pagination={null}
          title={() => (
            <Row justify="end" align="bottom">
              <Col>
                <Button
                  className="action-button button--import"
                  onClick={() => resetTasks()}
                  icon={<ReloadOutlined />}
                >
                  {local.translate('taskScheduling/update')}
                </Button>
              </Col>
            </Row>
          )}
        />
      ) : (
        <div className="no-data">
          <Alert
            message={local.translate('client/alert/warning')}
            type="warning"
            showIcon
            closable
            description={local.translate('taskScheduling/getData/error')}
          />
          <br />
          <Button
            type={'primary'}
            onClick={() => loadTasks()}
            icon={<ReloadOutlined />}
          >
            {local.translate('client/load/clientData')}
          </Button>
        </div>
      )}
    </div>
  );
}

export default TaskSchedulingPage;
