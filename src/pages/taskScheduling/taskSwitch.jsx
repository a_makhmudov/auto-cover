import React from 'react';
import { Switch } from 'antd';
import taskSchedulingService from '../../services/http/taskSchedulingService';

export const TaskSwitch = ({ record, text }) => {
  return (
    <Switch
      className="taskList-checker"
      onChange={() => taskSchedulingService.changeStatusTask(record.id)}
      defaultChecked={text}
    />
  );
};
