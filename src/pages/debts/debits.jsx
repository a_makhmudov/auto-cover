import React, { useEffect, useState } from 'react';
import { Alert, Button, Card, message, Modal } from 'antd';
import './debts.scss';
import DataTable from '../../components/dataTable/dataTable';
import { debitColumns } from '../../helpers/tableColumns';
import { useDispatch, useSelector } from 'react-redux';
import {
  setDebits,
  setDebitsCurrentPage,
  setDebtFilterData,
} from '../../store/reducers/debits';
import debitsService from '../../services/http/debitsService';
import local from '../../helpers/local';
import { ReloadOutlined } from '@ant-design/icons';
import Spinner from '../../components/spinner/spinner';
import { FilterFields } from '../../components/filterFields/filterFields';
import { debitFieldConstants } from './debitFieldConstants';

function DebtsPage() {
  const dispatch = useDispatch();
  const debitsList = useSelector((state) => state.debts);
  const [isFilterFieldModal, setFilterFieldModal] = useState(false); // state for filter modal window
  const [loading, setLoading] = useState(true);
  const currentPage = useSelector((state) => state.debts.currentPage);
  const debtsFilterParams = useSelector((state) => state.debts.filterData);
  // const [isAdminRole, setIsAdminRole] = useState(false);

  useEffect(async () => {
    await loadDebitsListPerPage(currentPage);

    /* This functional for hide row selection */
    // let jwt = localStorage.getItem('access_token');
    // const decode_jwt = jwtDecode(jwt);
    // if (decode_jwt.authorities.includes(ROLE_AUTOCOVER_ADMIN)) {
    //   setIsAdminRole(true);
    // } else setIsAdminRole(false);
    /* This functional for hide row selection */
  }, [currentPage, debtsFilterParams]);

  const getDebtsList = async () => {
    dispatch(setDebtFilterData({}));
    dispatch(setDebitsCurrentPage(0));
  };

  const filterData = async (filterData) => {
    const modifiedFilterData = {
      ...filterData,
      fio: filterData?.fio?.toUpperCase(),
    };
    dispatch(setDebtFilterData(modifiedFilterData));
    dispatch(setDebitsCurrentPage(0));
  };

  const loadDebitsListPerPage = async (page) => {
    try {
      setLoading(true);
      const response = await debitsService.getDebits({
        ...debtsFilterParams,
        page: page,
      });
      if (response.data.content.length <= 0) {
        message.error(local.translate('client/filter/noData'));
        dispatch(setDebits([]));
        setLoading(false);
        setFilterFieldModal(false);
      } else {
        setFilterFieldModal(false);
        setLoading(false);
        dispatch(setDebits(response.data));
        message.success(local.translate('client/clientList/success'));
      }
    } catch (e) {
      console.log(e);
    }
  };

  if (loading) {
    return <Spinner />;
  }
  return debitsList?.content?.length > 0 ? (
    <Card className="debts">
      <Modal
        title={local.translate('client/filter')}
        visible={isFilterFieldModal}
        onOk={() => getDebtsList()}
        onCancel={() => setFilterFieldModal(false)}
        footer={null}
      >
        <FilterFields
          defaultParams={debtsFilterParams}
          fields={debitFieldConstants}
          filter={filterData}
          closeFilterModal={() => setFilterFieldModal(false)}
        />
      </Modal>
      <DataTable
        rowKey={'creditId'}
        columns={debitColumns}
        data={debitsList?.content || null}
        title={
          <>
            <strong>{local.translate('debits/debtors')}:</strong>
            &nbsp;
            <span>
              {local.translate('debits/countDebtors')}{' '}
              {debitsList.totalElements}
            </span>
          </>
        }
        filterBtnClickHandler={() => setFilterFieldModal(true)}
        clearBtnClickHandler={() => getDebtsList()}
        // importBtnClickHandler={isAdminRole ? () => {} : null}
        pagination={{
          current: currentPage + 1,
          pageSize: 20,
          total: debitsList.totalElements || null,
          showSizeChanger: false,
          showLessItems: true,
          showQuickJumper: {
            goButton: (
              <Button>
                {local.translate('anor/pay/table/pagination/jumper')}
              </Button>
            ),
          },
          onChange: (page) => dispatch(setDebitsCurrentPage(page - 1)),
        }}
      />
    </Card>
  ) : (
    <div className="no-data">
      <Alert
        message={local.translate('client/alert/warning')}
        type="warning"
        showIcon
        closable
        description={'Данные не найдено'}
      />
      <br />
      <Button
        type={'primary'}
        onClick={() => getDebtsList()}
        icon={<ReloadOutlined />}
      >
        {local.translate('debt/filter/noData')}
      </Button>
    </div>
  );
}

export default DebtsPage;
