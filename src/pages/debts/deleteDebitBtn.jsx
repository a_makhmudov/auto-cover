import React, { useState } from 'react';
import { Button, message, Modal } from 'antd';
import { DeleteOutlined, WarningTwoTone } from '@ant-design/icons';
import debitsService from '../../services/http/debitsService';
import { useDispatch, useSelector } from 'react-redux';
import {
  setDebits,
  // setDebitsCurrentPage,
  // setDebtFilterData,
} from '../../store/reducers/debits';
import local from '../../helpers/local';

export const DeleteDebitBtn = ({ record }) => {
  const dispatch = useDispatch();
  const debtsFilterParams = useSelector((state) => state.debts.filterData);
  //---------------------- Modal settings --------------------------//
  const [isModalVisible, setIsModalVisible] = useState(false);
  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleOk = async () => {
    await deleteDebit();
    setIsModalVisible(false);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  //---------------------- Modal settings --------------------------//

  const deleteDebit = async () => {
    const response = await debitsService.deleteDebit(record.creditId);
    if (response.data) {
      const responseDebitList = await debitsService.getDebits(
        debtsFilterParams
      );
      dispatch(setDebits(responseDebitList.data));
      message.success(local.translate('debit/delete/success'));
      if (responseDebitList.data.length < 0) {
        const response = await debitsService.getDebits({});
        dispatch(setDebits(response.data));
      }
    } else message.success(local.translate('debit/delete/error'));
  };

  return (
    <>
      <Modal
        style={{ textAlign: 'center' }}
        title={local.translate('debit/confirmation/delete')}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <WarningTwoTone style={{ fontSize: '30px' }} twoToneColor={'#FF5254'} />
        &nbsp;
        <span>{local.translate('debit/confirm/delete')}</span>
      </Modal>

      <Button
        type={'default'}
        danger
        onClick={() => showModal()}
        className="btn"
      >
        <DeleteOutlined />
      </Button>
    </>
  );
};
