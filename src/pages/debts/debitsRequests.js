import debitsService from '../../services/http/debitsService';

export async function getDebits() {
  try {
    const response = await debitsService.getDebits();
    return response?.data;
  } catch (e) {
    console.log(e);
  }
}
