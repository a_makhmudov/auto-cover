import { FILTER_FIELD_TYPES } from '../../helpers/appConstants';

export const debitFieldConstants = [
  {
    label: 'ФИО:',
    placeholder: 'Введите ФИО',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'fio',
    reqKey: 'fio',
  },
  {
    label: 'Сумма долга:',
    placeholder: 'Введите сумму долга',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'debt',
    reqKey: 'debt',
  },
  {
    label: 'Паспорт серия \n(номер):',
    placeholder: 'Введите паспорт серия',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'passport',
    reqKey: 'passport',
  },
  {
    label: 'Кредит ID:',
    placeholder: 'Введите кредит ID',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'creditId',
    reqKey: 'creditId',
  },
  {
    label: 'Оплата:',
    placeholder: 'Введите оплату',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'payment',
    reqKey: 'payment',
  },
  {
    label: 'Остаток:',
    placeholder: 'Введите остаток',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'residue',
    reqKey: 'residue',
  },
  {
    label: 'Добавил(а):',
    placeholder: 'Введите имя инициатора',
    type: FILTER_FIELD_TYPES.TEXT,
    defaultValue: '',
    dataIndex: 'createdBy',
    reqKey: 'createdBy',
  },
  {
    label: 'Статус:',
    placeholder: 'Введите статуса',
    type: FILTER_FIELD_TYPES.SELECT,
    defaultValue: '',
    dataIndex: 'status',
    reqKey: 'status',
    options: {
      items: [
        {
          label: 'FINISHED',
          value: 'FINISHED',
        },
        {
          label: 'UNFINISHED',
          value: 'UNFINISHED',
        },
      ],
    },
  },
];
